<?php

namespace App\Http\Controllers;

use App\Models\Locations;
use App\Models\Vehicle;
use Illuminate\Http\Request;

class LocationsController extends Controller
{

    public function locationHistory(Request $request)
    {
        $imei = $request->imei;
        $start = $request->start;
        $end = $request->end;

        $locations = Locations::where('imei', $imei)->whereBetween('created_at', [$start, $end])
            ->groupBy('latitude', 'longitude')->get();
        return response()->json($locations, 200);
    }

    public function generateHistory(){
        $vehicles = Vehicle::all();

        return view('vehicles.generate_report', ['vehicles'=>$vehicles]);
    }
}
