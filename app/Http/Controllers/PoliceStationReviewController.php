<?php

namespace App\Http\Controllers;

use App\Models\PoliceStation;
use App\Models\PoliceStationReview;
use Illuminate\Http\Request;

class PoliceStationReviewController extends Controller
{

    public function store(Request $request)
    {
        $policeStationReview = new PoliceStationReview();
        $policeStation = PoliceStation::find($request->police_station_id);

        $policeStationReview->review = $request->review;
        $policeStationReview->rating = $request->rating;
        $policeStationReview->police_station_id = $request->police_station_id;
        $policeStationReview->user_id = $request->user_id;
        $policeStationReview->save();


        $avgRating = PoliceStationReview::where('police_station_id', $request->police_station_id)->avg('rating');
        $total_rating = PoliceStationReview::where('police_station_id', $request->police_station_id)->count();
        $policeStation->rating = $avgRating;
        $policeStation->nr_reviews = $total_rating;

        $policeStation->save();

        return response()->json($policeStationReview, 200);

    }

    public function getReviews($police_station_id){
        $policeStationReviews = PoliceStationReview::where('police_station_id', $police_station_id)->get();

        return response()->json($policeStationReviews, 200);
    }

    public function index(){
        $reviews = PoliceStationReview::all();

        return view('reviews.reviews', ['reviews'=>$reviews]);
    }
}
