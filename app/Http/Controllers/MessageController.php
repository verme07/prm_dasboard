<?php

namespace App\Http\Controllers;

use App\Models\Conversation;
use App\Models\Message;
use Illuminate\Http\Request;

class MessageController extends Controller
{

    public function index()
    {
        //
    }

    public function store(Request $request)
    {
        $message = new Message();

        $check_conversation = Conversation::where('police_station_id', $request->police_station_id)
            ->where('user_id', $request->user_id)->exists();

        if (!$check_conversation){
            $conversation = new Conversation();
            $conversation->chat_name = $request->chat_name;
            $conversation->police_station_id = $request->police_station_id;
            $conversation->user_id = $request->user_id;
            $conversation->save();

            $message->message = $request->message;
            $message->police_station_id = $request->police_station_id;
            $message->user_id = $request->user_id;
            $message->conversation_id = $conversation->id;
            $message->latitude = $request->latitude;
            $message->longitude = $request->longitude;
            $message->save();

            return response()->json($message, 200);
        }else{

            $conversation = Conversation::where('police_station_id', $request->police_station_id)
                ->where('user_id', $request->user_id)->first();
            $message->message = $request->message;
            $message->police_station_id = $request->police_station_id;
            $message->user_id = $request->user_id;
            $message->conversation_id = $conversation->id;
            $message->latitude = $request->latitude;
            $message->longitude = $request->longitude;
            $conversation->save();
            $message->save();

            return response()->json($message, 200);
        }

    }

    public function answer(Request $request)
    {
        $message = new Message();

        $check_conversation = Conversation::where('police_station_id', $request->police_station_id)
            ->where('user_id', $request->user_id)->exists();

        if (!$check_conversation){
            $conversation = new Conversation();
            $conversation->chat_name = $request->chat_name;
            $conversation->police_station_id = $request->police_station_id;
            $conversation->user_id = $request->user_id;

            $conversation->save();

            $message->message = $request->message;
            $message->police_station_id = $request->police_station_id;
            $message->user_id = $request->user_id;
            $message->conversation_id = $conversation->id;
            $message->sender = 1;
            $message->save();

            return redirect()->back();
        }else{

            $conversation = Conversation::where('police_station_id', $request->police_station_id)
                ->where('user_id', $request->user_id)->first();
            $message->message = $request->message;
            $message->police_station_id = $request->police_station_id;
            $message->user_id = $request->user_id;
            $message->conversation_id = $conversation->id;
            $message->sender = 1;
            $conversation->save();
            $message->save();

            return redirect()->back();
        }

    }


}
