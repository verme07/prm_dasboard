<?php

namespace App\Http\Controllers;

use App\Models\PoliceStation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class PoliceStationController extends Controller
{


    public function index()
    {
        //
    }

    public function addStation(){
        return view('police_stations.add_new_police_station');
    }

    public function list(){
        $police_stations = PoliceStation::all();

        return view('police_stations.list_police_stations', ['police_stations'=>$police_stations]);
    }

    public function policeStation($police_station_id){
        $police_station = PoliceStation::find($police_station_id);

        return view('police_stations.edit_police_station', ['police_station'=>$police_station]);
    }

    public function store(Request $request)
    {
        $station = new PoliceStation();
        $station->name = $request->name;
        $station->province = $request->province;
        $station->city = $request->city;
        $station->address = $request->address;
        $station->email = $request->email;
        $station->phone1 = $request->phone1;
        $station->phone2 = $request->phone2;
        $station->telephone = $request->telephone;
        $station->latitude = $request->latitude;
        $station->longitude = $request->longitude;
        $station->description = $request->description;

        $now = now();
        if ($request->file('photo1') != null){
            $photo1 = $request->file('photo1');
            $photo_name1 = $now->timestamp.'_p1'.'_'.$photo1->getClientOriginalName();
            $station->photo1 = $photo_name1;
        }
        if ($request->file('photo2') != null){
            $photo2 = $request->file('photo1');
            $photo_name2 = $now->timestamp.'_p2'.'_'.$photo2->getClientOriginalName();
            $station->photo2 = $photo_name2;
        }

        if ($request->file('photo3') != null){
            $photo3 = $request->file('photo1');
            $photo_name3 = $now->timestamp.'_p3'.'_'.$photo3->getClientOriginalName();
            $station->photo3 = $photo_name3;
        }

        if ($station->save()){
            if ($request->file('photo1') != null) {
                Storage::disk('public')->put($photo_name1, File::get($photo1));
            }
            if ($request->file('photo2') != null) {
                Storage::disk('public')->put($photo_name2, File::get($photo2));
            }
            if ($request->file('photo3') != null) {
                Storage::disk('public')->put($photo_name3, File::get($photo3));
            }
            return redirect()->route('list-police-station');
        }
    }

    public function edit(Request $request)
    {
        $station = PoliceStation::find($request->police_station_id);
        $station->name = $request->name;
        $station->province = $request->province;
        $station->city = $request->city;
        $station->address = $request->address;
        $station->email = $request->email;
        $station->phone1 = $request->phone1;
        $station->phone2 = $request->phone2;
        $station->telephone = $request->telephone;
        $station->latitude = $request->latitude;
        $station->longitude = $request->longitude;
        $station->description = $request->description;

        $now = now();
        if ($request->file('photo1') != null){
            $photo1 = $request->file('photo1');
            $photo_name1 = $now->timestamp.'_p1'.'_'.$photo1->getClientOriginalName();
            $station->photo1 = $photo_name1;
        }
        if ($request->file('photo2') != null){
            $photo2 = $request->file('photo1');
            $photo_name2 = $now->timestamp.'_p2'.'_'.$photo2->getClientOriginalName();
            $station->photo2 = $photo_name2;
        }

        if ($request->file('photo3') != null){
            $photo3 = $request->file('photo1');
            $photo_name3 = $now->timestamp.'_p3'.'_'.$photo3->getClientOriginalName();
            $station->photo3 = $photo_name3;
        }

        if ($station->save()){
            if ($request->file('photo1') != null) {
                Storage::disk('public')->put($photo_name1, File::get($photo1));
            }
            if ($request->file('photo2') != null) {
                Storage::disk('public')->put($photo_name2, File::get($photo2));
            }
            if ($request->file('photo3') != null) {
                Storage::disk('public')->put($photo_name3, File::get($photo3));
            }
            return redirect()->route('list-police-station');
        }
    }

    public function remove(Request $request){
        $station = PoliceStation::find($request->station_id);
        if ($station->delete()){
            return response()->json(['status' => 1], 200);
        }else{
            return response()->json(['status' => 0], 200);
        }
    }

    public function retrivePoliceStations(){
        $stations = PoliceStation::all();

        return response()->json($stations, 200);
    }
}
