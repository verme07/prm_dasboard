<?php

namespace App\Http\Controllers;

use App\Models\AutoDeDenuncia;
use App\Models\Document;

use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;

class DocumentController extends Controller
{

    public function index()
    {
        return view('documents.list_document');
    }

    public function addAutoDenuncia()
    {
        return view('documents.add_auto_denuncia');
    }

    public function listAutoDenuncia(){
        $documents = AutoDeDenuncia::all();

        return view('documents.documents', ['documents'=>$documents]);
    }


    public function getDocument($doc_id){
        $document = AutoDeDenuncia::find($doc_id);

        return view('documents.edit_auto_denuncia', ['document'=>$document]);
    }

    public function generatePDF($doc_id){
        $document = AutoDeDenuncia::find($doc_id);

        PDF::setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true]);
        view()->share(['document' => $document]);

        $pdf = PDF::loadView('docs_template.auto_denuncia');
        return $pdf->stream();

    }


    public function addAutoDeDenuncia(Request $request){
        $document = new AutoDeDenuncia();
        $document->complaint_nr = $request->complaint_nr;
        $document->squad_nr = $request->squad_nr;
        $document->province = $request->province;
        $document->zone = $request->zone;
        $document->day = $request->day;
        $document->month = $request->month;
        $document->year = $request->year;
        $document->time = $request->time;
        $document->name = $request->name;
        $document->civil_state = $request->civil_state;
        $document->gender = $request->gender;
        $document->age = $request->age;
        $document->day_birth = $request->day_birth;
        $document->month_birth = $request->month_birth;
        $document->year_birth = $request->year_birth;
        $document->name_father = $request->name_father;
        $document->name_mother = $request->name_mother;
        $document->natural = $request->natural;
        $document->district = $request->district;
        $document->natural_province = $request->natural_province;
        $document->nationality = $request->nationality;
        $document->profession = $request->profession;
        $document->residence = $request->residence;
        $document->complaint_quality = $request->complaint_quality;
        $document->case_day = $request->case_day;
        $document->case_month = $request->case_month;
        $document->case_year = $request->case_year;
        $document->case_time = $request->case_time;
        $document->case_place = $request->case_place;
        $document->case_address = $request->case_address;
        $document->case_description = $request->case_description;

        if ($document->save()){
            PDF::setOptions(['dpi' => 150]);
            view()->share(['document' => $document]);

            $pdf = PDF::loadView('docs_template.auto_denuncia');
            return $pdf->stream();
        }

    }

    public function edit(Request $request){
        $document = AutoDeDenuncia::find($request->doc_id);
        $document->complaint_nr = $request->complaint_nr;
        $document->squad_nr = $request->squad_nr;
        $document->province = $request->province;
        $document->zone = $request->zone;
        $document->day = $request->day;
        $document->month = $request->month;
        $document->year = $request->year;
        $document->time = $request->time;
        $document->name = $request->name;
        $document->civil_state = $request->civil_state;
        $document->gender = $request->gender;
        $document->age = $request->age;
        $document->day_birth = $request->day_birth;
        $document->month_birth = $request->month_birth;
        $document->year_birth = $request->year_birth;
        $document->name_father = $request->name_father;
        $document->name_mother = $request->name_mother;
        $document->natural = $request->natural;
        $document->district = $request->district;
        $document->natural_province = $request->natural_province;
        $document->nationality = $request->nationality;
        $document->profession = $request->profession;
        $document->residence = $request->residence;
        $document->complaint_quality = $request->complaint_quality;
        $document->case_day = $request->case_day;
        $document->case_month = $request->case_month;
        $document->case_year = $request->case_year;
        $document->case_time = $request->case_time;
        $document->case_place = $request->case_place;
        $document->case_address = $request->case_address;
        $document->case_description = $request->case_description;

        if ($document->save()){
            PDF::setOptions(['dpi' => 150]);
            view()->share(['document' => $document]);

            $pdf = PDF::loadView('docs_template.auto_denuncia');
            return $pdf->stream();
        }

    }

    public function remove(Request $request){
        $document = AutoDeDenuncia::find($request->doc_id);
        if ($document->delete()){
            return response()->json(['status' => 1], 200);
        }else{
            return response()->json(['status' => 0], 200);
        }
    }

    public function duplicateEdit($doc_id){
        $document = AutoDeDenuncia::find($doc_id);

        return view('documents.duplicate_auto_denuncia', ['document'=>$document]);
    }
}
