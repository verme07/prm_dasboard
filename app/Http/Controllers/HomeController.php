<?php

namespace App\Http\Controllers;

use App\Models\AutoDeDenuncia;
use App\Models\PoliceStation;
use App\Models\Vehicle;
use Illuminate\Http\Request;
use stdClass;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $totals = new stdClass();
        $totals->police_stations =  PoliceStation::count();
        $totals->vehicles =  Vehicle::count();
        $totals->docs =  AutoDeDenuncia::count();
        $police_stations = PoliceStation::all();
        return view('dashboard.index', ['totals'=>$totals, 'police_stations'=>$police_stations]);
    }
}
