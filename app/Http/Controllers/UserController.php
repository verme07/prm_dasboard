<?php

namespace App\Http\Controllers;

use App\Models\PoliceStation;
use App\Models\User;
use http\Env\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function addUser()
    {
        $polices_stations = PoliceStation::all();
        return view('users.add_user', ['polices_stations' => $polices_stations]);
    }

    public function store(Request $request)
    {

        $user = new User();
        $user->name = $request->name;
        $user->username = $request->username;
        $user->type = $request->permission;
        $user->email = $request->email;
        $user->police_station_id = $request->police_station_id;

        $user->password = Hash::make($request->password);

        $user->save();

        return redirect()->route('list-user');
    }


    public function storeApi(Request $request)
    {

        $user_exist = User::where('username', $request->phone)->exists();
        if ($user_exist) {

            return response()->json(['status' => -1, 'message' => 'User Already Exists'], 200);
        } else {
            $user = new User();
            $user->name = $request->name;
            $user->username = $request->phone;
            $user->type = 0;
            $user->email = $request->email;

            $user->password = Hash::make($request->password);

            $user->save();

            return response()->json($user, 200);
        }

    }

    public function editApi(Request $request)
    {

        $user = User::find($request->user_id);
        $user->name = $request->name;
        $user->username = $request->phone;

        $user->email = $request->email;

        $user->save();

        return response()->json($user, 200);
    }

    public function editApiIOS(Request $request)
    {

        $user = User::find($request->user_id);
        $user->name = $request->name;
        $user->username = $request->phone;

        $user->email = $request->email;
        $user->password = Hash::make($request->password);

        $user->save();

        return response()->json($user, 200);
    }




    public function updatePassApi(Request $request){
        $user = User::find($request->user_id);
        $current_password = $user->password;

        if(Hash::check($request->old_password, $current_password)){
            $user->password = Hash::make($request->password);
            $user->save();
            return response()->json($user, 200);
        }else{
            return response()->json(['status' => -1, 'message' => 'Invalid Password'], 200);

        }
    }



    public function loginApi(Request $request)
    {
        $user = User::where('username', $request->phone)->first();

        if (Auth::attempt(['email' => $user->email, 'password' => $request['password']])) {
            return response()->json($user, 200);
        } else {
            return response()->json(['message' => 'Your credentials are incorrect'], 401);

        }

    }




    public function edit(Request $request)
    {

        $user = User::find($request->user_id);
        $user->name = $request->name;
        $user->username = $request->username;
        $user->email = $request->email;
        $user->police_station_id = $request->police_station_id;

        $user->save();

        return redirect()->route('list-user');
    }


    public function listUser()
    {
        $users = User::whereIn('type', [1, 2])->get();

        return view('users.list_user', ['users' => $users]);
    }

    public function listAppUsers()
    {
        $users = User::whereIn('type', [0])->get();

        return view('users.app_users', ['users' => $users]);
    }

    public function editUser($user_id)
    {
        $user = User::find($user_id);
        $polices_stations = PoliceStation::all();
        return view('users.edit_user', ['user' => $user, 'polices_stations' => $polices_stations]);
    }

    public function remove(Request $request)
    {
        $user = User::find($request->user_id);
        if ($user->delete()) {
            return response()->json(['status' => 1], 200);
        } else {
            return response()->json(['status' => 0], 200);
        }
    }

}
