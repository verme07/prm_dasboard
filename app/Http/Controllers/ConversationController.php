<?php

namespace App\Http\Controllers;

use App\Models\Conversation;
use App\Models\Message;

use Illuminate\Http\Request;

class ConversationController extends Controller
{

    public function index($chat_id)
    {
        $conversations = Conversation::all();
        $conversation = Conversation::find($chat_id);
        $chats = Message::where('conversation_id', $chat_id)->get();

        return view('chat.chat', ['conversations'=>$conversations, 'conversation'=>$conversation,'chats'=>$chats]);
    }

    public function getConversations(){
        $conversations = Conversation::all();

        return response()->json($conversations, 200);
    }

    public function getMessageChat($chat_id){
        $chats = Message::where('conversation_id', $chat_id)->get();

        return response()->json($chats, 200);
    }

    public function getSendMessages($user_id){
        $messages = Message::with('station:id,name', 'user:id,name')->where([['user_id', $user_id], ['sender', 0]])->get();

        return response()->json($messages, 200);
    }

    public function getReceivedMessages($user_id){
        $messages = Message::with('station:id,name', 'user:id,name')->where([['user_id', $user_id], ['sender', 1]])->get();

        return response()->json($messages, 200);
    }

}
