<?php

namespace App\Http\Controllers;

use App\Models\PoliceStation;
use App\Models\Vehicle;
use Illuminate\Http\Request;

class VehicleController extends Controller
{

    public function index()
    {
        //
    }

    public function addVehicle(){

        $polices_stations = PoliceStation::all();

        return view('vehicles.add_new_vehicle', ['polices_stations'=>$polices_stations]);
    }

    public function list(){
        $vehicles = Vehicle::all();

        return view('vehicles.list_vehicles', ['vehicles'=>$vehicles]);
    }

    public function map(){
        $vehicles = Vehicle::all();

        return view('vehicles.vehicle_map', ['vehicles'=>$vehicles]);
    }



    public function viewCarEdit($vehicle_id){
        $vehicle = Vehicle::find($vehicle_id);
        $polices_stations = PoliceStation::all();
        return view('vehicles.edit_vehicle', ['vehicle'=>$vehicle, 'polices_stations'=>$polices_stations]);

    }
    public function store(Request $request)
    {
        $vehicle = new Vehicle();
        $vehicle->imei = $request->imei;
        $vehicle->engine_nr = $request->engine_nr;
        $vehicle->brand = $request->brand;
        $vehicle->reference = $request->reference;
        $vehicle->number_plate = $request->number_plate;
        $vehicle->space = $request->space;
        $vehicle->gas_type = $request->gas_type;
        $vehicle->police_station_id = $request->police_station_id;

        if ($vehicle->save()){
            return redirect()->route('list-vehicle');
        }
    }

    public function edit(Request $request)
    {
        $vehicle = Vehicle::find($request->vehicle_id);
        $vehicle->imei = $request->imei;
        $vehicle->engine_nr = $request->engine_nr;
        $vehicle->brand = $request->brand;
        $vehicle->reference = $request->reference;
        $vehicle->number_plate = $request->number_plate;
        $vehicle->space = $request->space;
        $vehicle->gas_type = $request->gas_type;
        $vehicle->police_station_id = $request->police_station_id;

        if ($vehicle->save()){
            return redirect()->route('list-vehicle');
        }
    }

    public function remove(Request $request){
        $vehicle = Vehicle::find($request->vehicle_id);
        if ($vehicle->delete()){
            return response()->json(['status' => 1], 200);
        }else{
            return response()->json(['status' => 0], 200);
        }
    }

}
