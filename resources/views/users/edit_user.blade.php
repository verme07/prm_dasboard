@extends('layouts.master')

@section('css')

@endsection

@section('content')
    <div class="container-fluid">

        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
                    <h4 class="page-title">Editar Utilizador</h4>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0);">PRM</a></li>
                        <li class="breadcrumb-item"><a href="javascript:void(0);">Utilizadores</a></li>
                        <li class="breadcrumb-item active">Editar Utilizador</li>
                    </ol>
                </div>
            </div>
        </div>
        <!-- end row -->

        <div class="row">
            <div class="col-lg-12">
                <div class="card m-b-20">
                    <div class="card-body">

                        <h4 class="mt-0 header-title">Editar Utilizador</h4>

                        <form class="" method="POST" enctype="multipart/form-data" action="{{ route('save-user-edit') }}">
                            @csrf
                            <div class="row">
                                <div class="col-6">
                                    <div class="form-group">
                                        <label>Nome <span>*</span></label>
                                        <input name="user_id" id="user_id" value="{{ $user->id }}" type="hidden" class="form-control" required/>
                                        <input name="name" id="name" value="{{ $user->name }}" type="text" class="form-control" required/>
                                    </div>

                                    <div class="form-group">
                                        <label>NIP <span>*</span></label>
                                        <input name="username" id="username" value="{{ $user->username }}"  type="text" class="form-control" required/>
                                    </div>
                                    <div class="form-group">
                                        <label>Email <span>*</span></label>
                                        <input name="email" id="email" value="{{ $user->email }}"  type="text" class="form-control" required/>
                                    </div>

                                </div>

                                <div class="col-6">

                                    <div class="form-group">
                                        <label>Posto Policial </label>
                                        <select class="form-control" name="police_station_id" id="police_station_id" required>
                                            <option>Selecione o posto policial ao qual pertence a viatura</option>
                                            @foreach($polices_stations as $police_station)
                                                <option value="{{ $police_station->id }}" {{ $police_station->id == $user->id ? 'selected="selected"' : '' }}>{{ $police_station->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Permissões </label>
                                        <select class="form-control" name="permission" id="permission" required>
                                            <option>Selecione o nivel de acesso</option>
                                            <option value="normal">Normal</option>
                                            <option value="admin">Admin</option>

                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label>Password <span>*</span></label>
                                        <input name="password" id="password" type="password" class="form-control" required/>
                                    </div>

                                </div>
                            </div>
                            <div class="form-group">
                                <div>
                                    <button type="submit" class="btn btn-primary waves-effect waves-light">
                                        Adicionar
                                    </button>
                                </div>
                            </div>
                        </form>

                    </div>
                </div>
            </div> <!-- end col -->

        </div> <!-- end row -->

    </div> <!-- container-fluid -->
@endsection

@section('script')
    <!-- Parsley js -->
    <script src="{{ URL::asset('assets/plugins/parsleyjs/parsley.min.js')}}"></script>
@endsection

@section('script-bottom')
    <script>
        $(document).ready(function() {
            $('form').parsley();
        });
    </script>
@endsection
