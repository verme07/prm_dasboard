@extends('layouts.master')

@section('css')
    <!-- DataTables -->
    <link href="{{ asset('assets/plugins/datatables/dataTables.bootstrap4.min.css')}}" rel="stylesheet"
          type="text/css"/>
    <link href="{{ asset('assets/plugins/datatables/buttons.bootstrap4.min.css')}}" rel="stylesheet"
          type="text/css"/>
    <!-- Responsive datatable examples -->
    <link href="{{ asset('assets/plugins/datatables/responsive.bootstrap4.min.css')}}" rel="stylesheet"
          type="text/css"/>

    <link href="{{ asset('assets/plugins/ion-rangeslider/ion.rangeSlider.skinModern.css')}}" rel="stylesheet"
          type="text/css"/>

    <style>
        .checked {
            color: orange;
        }
    </style>
@endsection

@section('content')
    <div class="container-fluid">

        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
                    <h4 class="page-title">Classificações</h4>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0);">PRM</a></li>
                        <li class="breadcrumb-item"><a href="javascript:void(0);">Classificações</a></li>
                        <li class="breadcrumb-item active">Listar Classificações</li>
                    </ol>
                </div>
            </div>
        </div>
        <!-- end row -->

        <div class="row">
            <div class="col-12">
                <div class="card m-b-20">
                    <div class="card-body">

                        <h4 class="mt-0 header-title">Classificações</h4>

                        <table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap"
                               style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                            <thead>
                            <tr>
                                <th>#ID</th>
                                <th>Utilizador</th>
                                <th>Classificação</th>
                                <th>Posto Policial</th>
                                <th>Mensagem</th>

                            </tr>
                            </thead>
                            <tbody>
                            @foreach($reviews as $review)
                                <tr>
                                    <td>{{ $review->id }}</td>
                                    <td>{{ $review->user->name }}</td>
                                    <td>
                                        <span class="fa fa-star @if($review->rating >= 1) checked @endif"></span>
                                        <span class="fa fa-star @if($review->rating >= 2) checked @endif"></span>
                                        <span class="fa fa-star @if($review->rating >= 3) checked @endif"></span>
                                        <span class="fa fa-star @if($review->rating >= 4) checked @endif"></span>
                                        <span class="fa fa-star @if($review->rating >= 5) checked @endif"></span>
                                    </td>
                                    <td>{{ $review->station->name }}</td>
                                    <td>{{ $review->review }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div> <!-- end col -->
        </div> <!-- end row -->

    </div> <!-- container-fluid -->
@endsection

@section('script')
    <!-- Required datatable js -->
    <script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js')}}"></script>
    <script src="{{ asset('assets/plugins/datatables/dataTables.bootstrap4.min.js')}}"></script>
    <!-- Buttons examples -->
    <script src="{{ asset('assets/plugins/datatables/dataTables.buttons.min.js')}}"></script>
    <script src="{{ asset('assets/plugins/datatables/buttons.bootstrap4.min.js')}}"></script>
    <script src="{{ asset('assets/plugins/datatables/jszip.min.js')}}"></script>
    <script src="{{ asset('assets/plugins/datatables/pdfmake.min.js')}}"></script>
    <script src="{{ asset('assets/plugins/datatables/vfs_fonts.js')}}"></script>
    <script src="{{ asset('assets/plugins/datatables/buttons.html5.min.js')}}"></script>
    <script src="{{ asset('assets/plugins/datatables/buttons.print.min.js')}}"></script>
    <script src="{{ asset('assets/plugins/datatables/buttons.colVis.min.js')}}"></script>
    <!-- Responsive examples -->
    <script src="{{ asset('assets/plugins/datatables/dataTables.responsive.min.js')}}"></script>
    <script src="{{ asset('assets/plugins/datatables/responsive.bootstrap4.min.js')}}"></script>

    <!-- Datatable init js -->
    <script src="{{ asset('assets/pages/datatables.init.js')}}"></script>
@endsection
