@extends('layouts.master')
@section('css')
    <!-- DataTables -->
    <link href="{{ asset('assets/plugins/datatables/dataTables.bootstrap4.min.css')}}" rel="stylesheet"
          type="text/css"/>
    <link href="{{ asset('assets/plugins/datatables/buttons.bootstrap4.min.css')}}" rel="stylesheet"
          type="text/css"/>
    <!-- Responsive datatable examples -->
    <link href="{{ asset('assets/plugins/datatables/responsive.bootstrap4.min.css')}}" rel="stylesheet"
          type="text/css"/>

    <link href="{{ asset('assets/plugins/ion-rangeslider/ion.rangeSlider.skinModern.css')}}" rel="stylesheet"
          type="text/css"/>
    <link href="{{ asset('assets/css/chat.css')}}" rel="stylesheet"
          type="text/css"/>
@endsection


@section('content')
    <div class="container-fluid">

        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
                    <h4 class="page-title">Chat</h4>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0);">PRM</a></li>
                        <li class="breadcrumb-item"><a href="javascript:void(0);">Chat</a></li>
                        <li class="breadcrumb-item active">Listar Chats</li>
                    </ol>
                </div>
            </div>
        </div>

        <!-- end row -->
        <div class="row">
            <div class="col-12">
                <div class="card m-b-0">
                    <!-- .chat-row -->
                    <div class="chat-main-box">
                        <!-- .chat-left-panel -->
                        <div class="chat-left-aside">
                            <div class="open-panel"><i class="ti-angle-right"></i></div>
                            <div class="chat-left-inner">
                                <div class="form-material">
                                    <input class="form-control p-20" type="text" placeholder="Procurar Conversa">
                                </div>
                                <ul class="chatonline style-none ">
                                    @foreach($conversations as $conversation)
                                        <li>
                                            <a href="{{ route('chat', ['chat_id'=>$conversation->id]) }}" class="{{ (request()->is('chat/'.$conversation->id)) ? 'active' : '' }}"><img
                                                    src="{{ asset('assets/images/user.png') }}" alt="user-img"
                                                    class="img-circle"> <span>{{ $conversation->user->name }} <small
                                                        class="text-danger">{{ $conversation->station->name }}</small></span></a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                        <!-- .chat-left-panel -->
                        <!-- .chat-right-panel -->
                        <div class="chat-right-aside">
                            <div class="chat-main-header">
                                <div class="p-20 b-b">
                                    <h5 class="box-title">Mensagens da Conversa</h5>
                                </div>
                            </div>
                            <div class="chat-rbox">
                                <ul class="chat-list p-20">
                                    @foreach($chats as $chat)
                                        @if($chat->sender == 0)
                                            <li>
                                                <div class="chat-img"><img src="{{ asset('assets/images/user.png') }}"
                                                                           alt="user"/></div>
                                                <div class="chat-content">
                                                    <h6>{{ $chat->user->name }}</h6>
                                                    <div class="box bg-light-info">{{ $chat->message }}
                                                    </div>
                                                </div>
                                                <div class="chat-time">{{ $chat->created_at }}</div>
                                            </li>
                                        @else
                                            <li class="reverse">

                                                <div class="chat-content">
                                                    <h6>{{ $chat->station->name }}</h6>
                                                    <div class="box bg-light-inverse">{{ $chat->message }}
                                                    </div>
                                                </div>
                                                <div class="chat-img"><img src="{{ asset('assets/images/user.png') }}"
                                                                           alt="user"/></div>
                                                <div class="chat-time">{{ $chat->created_at }}</div>
                                            </li>
                                    @endif
                                @endforeach
                                <!--chat Row -->
                                </ul>
                            </div>
                            <div class="card-body b-t">
                                <form method="post" enctype="multipart/form-data"  action="{{ route('answer-message') }}">
                                    @csrf
                                    <div class="row">
                                        <div class="col-8">
                                            <input type="hidden" name="police_station_id" value="{{ $conversation->police_station_id }}">
                                            <input type="hidden" name="user_id" value="{{ $conversation->user_id }}">
                                            <textarea name="message" placeholder="Responder a Mensagem"
                                                      class="form-control b-0" required></textarea>
                                        </div>
                                        <div class="col-4 text-right">
                                            <button type="submit" class="btn btn-info btn-circle btn-lg"><i
                                                    class="fas fa-paper-plane"></i></button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <!-- .chat-right-panel -->
                    </div>
                    <!-- /.chat-row -->
                </div>
            </div>
        </div>


    </div> <!-- container-fluid -->
@endsection

@section('script')
    <!-- Required datatable js -->
    <script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js')}}"></script>
    <script src="{{ asset('assets/plugins/datatables/dataTables.bootstrap4.min.js')}}"></script>
    <!-- Buttons examples -->
    <script src="{{ asset('assets/plugins/datatables/dataTables.buttons.min.js')}}"></script>
    <script src="{{ asset('assets/plugins/datatables/buttons.bootstrap4.min.js')}}"></script>
    <script src="{{ asset('assets/plugins/datatables/jszip.min.js')}}"></script>
    <script src="{{ asset('assets/plugins/datatables/pdfmake.min.js')}}"></script>
    <script src="{{ asset('assets/plugins/datatables/vfs_fonts.js')}}"></script>
    <script src="{{ asset('assets/plugins/datatables/buttons.html5.min.js')}}"></script>
    <script src="{{ asset('assets/plugins/datatables/buttons.print.min.js')}}"></script>
    <script src="{{ asset('assets/plugins/datatables/buttons.colVis.min.js')}}"></script>
    <!-- Responsive examples -->
    <script src="{{ asset('assets/plugins/datatables/dataTables.responsive.min.js')}}"></script>
    <script src="{{ asset('assets/plugins/datatables/responsive.bootstrap4.min.js')}}"></script>

    <!-- Datatable init js -->
    <script src="{{ asset('assets/pages/datatables.init.js')}}"></script>
@endsection
