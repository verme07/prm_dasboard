@extends('layouts.master')

@section('css')
    <!-- DataTables -->
    <link href="{{ asset('assets/plugins/datatables/dataTables.bootstrap4.min.css')}}" rel="stylesheet"
          type="text/css"/>
    <link href="{{ asset('assets/plugins/datatables/buttons.bootstrap4.min.css')}}" rel="stylesheet"
          type="text/css"/>
    <!-- Responsive datatable examples -->
    <link href="{{ asset('assets/plugins/datatables/responsive.bootstrap4.min.css')}}" rel="stylesheet"
          type="text/css"/>

    <link href="{{ asset('assets/plugins/ion-rangeslider/ion.rangeSlider.skinModern.css')}}" rel="stylesheet"
          type="text/css"/>
@endsection

@section('content')
    <div class="container-fluid">

        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
                    <h4 class="page-title">Viaturas</h4>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0);">PRM</a></li>
                        <li class="breadcrumb-item"><a href="javascript:void(0);">Postos Policias</a></li>
                        <li class="breadcrumb-item active">Listar Postos Policias</li>
                    </ol>
                </div>
            </div>
        </div>
        <!-- end row -->

        <div class="row">
            <div class="col-12">
                <div class="card m-b-20">
                    <div class="card-body">

                        <h4 class="mt-0 header-title">Lista de Viaturas</h4>

                        <table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap"
                               style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                            <thead>
                            <tr>
                                <th>#IMEI</th>
                                <th>Marca</th>
                                <th>Modelo</th>
                                <th>Matricula</th>
                                <th>Estado do GPS</th>
                                <th>Editar</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($vehicles as $vehicle)
                                <tr>
                                    <td>{{ $vehicle->imei }}</td>
                                    <td>{{ $vehicle->brand }}</td>
                                    <td>{{ $vehicle->reference }}</td>
                                    <td>{{ $vehicle->number_plate }}</td>
                                    <td>@if($vehicle->status)
                                            Activado
                                        @else
                                            Desactivado
                                        @endif
                                    </td>

                                    <td>
                                        <div class="dropdown mo-mb-2" style="text-align: center">
                                            <button class="btn btn-secondary dropdown-toggle" type="button"
                                                    id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true"
                                                    aria-expanded="false">
                                                Editar
                                            </button>
                                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                                <a class="dropdown-item"
                                                   href="{{ route('edit-vehicle', ['vehicle_id'=>$vehicle->id]) }}">Editar Viatura</a>
                                                <button class="dropdown-item" onclick="remove({{$vehicle->id}})">
                                                    Remover
                                                </button>
                                            </div>
                                        </div>
                                    </td>

                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div> <!-- end col -->
        </div> <!-- end row -->
        <div class="modal fade bs-example-modal-center" id="removerModal" tabindex="-1" role="dialog"
             aria-labelledby="mySmallModalLabel"
             aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title mt-0">Deseja Remover Esta Viatura?</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    </div>
                    <div class="modal-body">
                        <button type="button" class="btn btn-danger" id="delete-btn">Remover</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->

    </div> <!-- container-fluid -->
@endsection

@section('script')
    <script>
        function remove(vehicle_id) {

            $('#removerModal').modal({backdrop: 'static', keyboard: false})
                .on('click', '#delete-btn', function () {
                    let user = {
                        _token: "{{ csrf_token() }}",
                        vehicle_id: vehicle_id,
                    }
                    $.ajax({
                        method: 'POST',
                        url: "{{ route('remove-vehicle') }}",
                        dataType: 'text',
                        contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
                        data: user,
                        complete: function (data) {
                            console.log("success");
                        },
                        success: function (response) {

                            let resposta = JSON.parse(response)
                            console.log(resposta)
                            if (resposta.status == 1) {
                                location.reload();
                            } else {
                                console.log("Erro")
                            }
                            ;

                        }, error: function (xhr, type, exception) {
                            console.log("Error " + xhr, type, exception + "Exception ");
                            //$('#email_error').css('display', 'block')

                        }
                    });
                })
        }
    </script>
    <!-- Required datatable js -->
    <script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js')}}"></script>
    <script src="{{ asset('assets/plugins/datatables/dataTables.bootstrap4.min.js')}}"></script>
    <!-- Buttons examples -->
    <script src="{{ asset('assets/plugins/datatables/dataTables.buttons.min.js')}}"></script>
    <script src="{{ asset('assets/plugins/datatables/buttons.bootstrap4.min.js')}}"></script>
    <script src="{{ asset('assets/plugins/datatables/jszip.min.js')}}"></script>
    <script src="{{ asset('assets/plugins/datatables/pdfmake.min.js')}}"></script>
    <script src="{{ asset('assets/plugins/datatables/vfs_fonts.js')}}"></script>
    <script src="{{ asset('assets/plugins/datatables/buttons.html5.min.js')}}"></script>
    <script src="{{ asset('assets/plugins/datatables/buttons.print.min.js')}}"></script>
    <script src="{{ asset('assets/plugins/datatables/buttons.colVis.min.js')}}"></script>
    <!-- Responsive examples -->
    <script src="{{ asset('assets/plugins/datatables/dataTables.responsive.min.js')}}"></script>
    <script src="{{ asset('assets/plugins/datatables/responsive.bootstrap4.min.js')}}"></script>

    <!-- Datatable init js -->
    <script src="{{ asset('assets/pages/datatables.init.js')}}"></script>
@endsection
