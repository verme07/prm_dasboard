@extends('layouts.master')

@section('css')

    <script src="https://polyfill.io/v3/polyfill.min.js?features=default"></script>
    <script
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAJloUT_O0qVDJHVr0pBkSrR2UV4GT_Yik&callback=initMap&libraries=&v=weekly"
        defer></script>
    <style>
        .lds-dual-ring.hidden {
            display: none;
        }
        .lds-dual-ring {
            display: inline-block;
            width: 80px;
            height: 80px;
        }
        .lds-dual-ring:after {
            content: " ";
            display: block;
            width: 64px;
            height: 64px;
            margin: 5% auto;
            border-radius: 50%;
            border: 6px solid #fff;
            border-color: #fff transparent #fff transparent;
            animation: lds-dual-ring 1.2s linear infinite;
        }
        @keyframes lds-dual-ring {
            0% {
                transform: rotate(0deg);
            }
            100% {
                transform: rotate(360deg);
            }
        }


        .overlay {
            position: fixed;
            top: 0;
            left: 0;
            width: 100%;
            height: 100vh;
            background: rgba(0,0,0,.8);
            z-index: 999;
            opacity: 1;
            transition: all 0.5s;
        }
        .gm-style .gm-style-iw-c {
            padding: 0px;
            overflow: unset;
        }

        #map {
            height: 100%
        }

        #planRouteMode {
            width: 78px;
            height: 24px;
            line-height: 24px;
            border: none;
            outline: none;
            float: right;
        }

        #getLocation {
            width: auto;
            height: 38px;
            padding: 5px;
            margin: 0;
            text-align: center;
            position: absolute;
            right: 355px;
            bottom: 120px;
        }

        .btn-getLocation {
            box-shadow: rgba(0, 0, 0, 0.3) 0px 1px 4px -1px;
            border-radius: 2px;
            cursor: pointer;
            background-color: #fff;
            width: 28px;
            height: 28px;
            right: 0;
            bottom: 5px;
        }

        .btn-getLocation i {
            position: relative;
            top: 4px;
            line-height: 20px;
            width: 20px;
            height: 20px;
            font-size: 18px;
            color: #666;
        }

        #myLocation {
            font-size: 12px;
            vertical-align: text-top;
        }

        @media (max-width: 992px) and (min-width: 520px) {
            #map {
                width: 100%;
                height: 500px;
            }

            #getLocation {
                top: 341px;
                right: 5px;
            }
        }

        @media (max-width: 520px) {
            #map {
                width: 100%;
                height: 200px;
            }

            #getLocation {
                top: 192px;
                bottom: 0;
                right: 5px;
            }
        }

        .badge-gray {
            background: #bdbdbd;
        }

        .badge-darkgreen {
            background: #00c853;
        }

        .badge-lightgreen {
            background: #64dd17;
        }

        .badge-yellow {
            background: #ffca00;
        }

        .badge-orange {
            background: #ff8300;
        }

        .badge-red {
            background: #f00;
        }

        .rating {
            width: 32px;
            height: 20px;
            line-height: 20px;
            padding: 1px;
        }

        .placeName {
            overflow: hidden;
            text-overflow: ellipsis;
            white-space: pre-line;
            height: 16px;
            width: 255px;
            display: block;
        }

        .map-list-items {
            overflow-y: scroll;
            width: 200px;
            position: relative;
            height: calc(100% - 70px);
            padding: 0;
            background-color: white;
        }

        .panel-success {
            position: absolute;
            right: 0;
            top: 0;
            bottom: 0;
            margin: 0;
            box-shadow: 0 0 20px rgba(0, 0, 0, 0.3);
            border: none;
            margin-top: 70px;
        }

        .map-panel-title {
            background-color: #ea922d !important;
            color: #fff !important;
            border: none;
            padding: 11px 15px 9px 15px;
            width: 200px;
            height: 70px;
        }

        .map-panel-title h4 {
            margin: 6px 0;
        }

        .panel, .map-panel-title, .list-group-item:last-child, .list-group-item:first-child {
            border-radius: 0;
        }

        .list-group-item {
            height: 40px;
            margin-bottom: 0;
            border-left: none;
            border-right: none;
            border-top: none;
            border-bottom: 1px solid #ddd;
            cursor: pointer;
        }

        @media (max-width: 992px) and (min-width: 520px) {
            form .form-group {
                float: left;
            }

            form .form-group:nth-child(2) {
                margin-left: 10px;
            }

            .form-control {
                display: inline-block;
                width: auto;
            }

            .panel-success {
                position: static;
                right: inherit;
                top: inherit;
                bottom: inherit;
                height: calc(100% - 500px);
            }

            .map-panel-title {
                width: 100%;
            }

            .map-list-items {
                width: 100%;
                position: static;
            }
        }

        @media (max-width: 520px) {
            .panel-success {
                position: static;
                right: inherit;
                top: inherit;
                bottom: inherit;
                height: calc(100% - 200px);
            }

            .map-panel-title {
                width: 100%;
            }

            .map-list-items {
                width: 100%;
                position: static;
            }
        }

        #origin-input, #destination-input {
            background-color: #fff;
            font-family: Roboto;
            font-size: 15px;
            font-weight: 300;
            margin-left: 12px;
            padding: 0 11px 0 13px;
            text-overflow: ellipsis;
            width: 200px;
        }

        #origin-input:focus, #destination-input:focus {
            border-color: #4d90fe;
        }

        #mode-selector {
            color: #fff;
            background-color: #4d90fe;
            margin-left: 12px;
            padding: 5px 11px 0px 11px;
        }

        #mode-selector label {
            font-size: 13px;
        }

        input::-moz-placeholder {
            color: #999;
            opacity: 1
        }

        input:-ms-input-placeholder {
            color: #999
        }

        input::-webkit-input-placeholder {
            color: #999
        }

        #omnibox {
            position: absolute;
            margin: 10px;
            z-index: 10;
            top: 0;
            margin-top: 75px
        }

        #searchbox_form {
            display: inline-block;
        }

        .searchbox.suggestions-shown {
            border-radius: 2px 2px 0 0;
        }

        .searchbox {
            position: relative;
            background: #fff;
            border-radius: 2px;
            box-sizing: border-box;
            width: 424px;
            height: 48px;
            border-bottom: 1px solid transparent;
            padding: 12px 2px 11px 16px;
            box-shadow: 0 2px 4px rgba(0, 0, 0, 0.2), 0 -1px 0px rgba(0, 0, 0, 0.02);
        }

        .searchbox #radius {
            border: none;
            outline: none;
            font-size: 15px;
            width: 56px;
            height: 24px;
            line-height: 24px;
        }

        .searchbox #keyword {
            border: none;
            padding: 0;
            outline: none;
            font-size: 15px;
            width: 210px;
            height: 24px;
            line-height: 24px;
        }

        .searchbox-searchbtn-container {
            position: absolute;
            right: 5px;
            top: 0;
        }

        .searchbox-searchbtn-container::before, .searchbox-searchbtn-container::after {
            content: "";
            position: absolute;
            top: 10px;
            border-left: 1px solid #ddd;
            height: 28px;
        }

        .searchbox-searchbtn-container::after {
            right: 0;
        }

        #searchbox-searchbtn {
            display: block;
            cursor: pointer;
            padding: 12px;
            margin: 0;
            border: 0;
            outline: 0;
            background: transparent;
            list-style: none;
        }

        #searchbox-searchbtn::before {
            content: '';
            display: block;
            width: 24px;
            height: 24px;
            background: url(https://maps.gstatic.com/tactile/omnibox/quantum_search_button-20150825-1x.png);
        }

        @media (max-width: 520px) {
            #omnibox {
                margin: 0 auto;
                width: 100%;
            }

            .searchbox {
                width: 100%;
                height: 40px;
                padding: 8px 4px 8px 8px;
            }

            #searchbox_form {
                width: calc(100% - 130px);
            }

            .searchbox #keyword {
                width: calc(100% - 60px);
            }

            #searchbox-searchbtn {
                padding: 8px 10px;
            }

            .searchbox-searchbtn-container {
                position: absolute;
                right: 88px;
                top: 0;
            }

            .searchbox-searchbtn-container::before, .searchbox-searchbtn-container::after {
                top: 6px;
            }
        }

        .place_suggestions {
            background-color: #fff;
            border-radius: 0 0 2px 2px;
            box-shadow: 0 2px 4px rgba(0, 0, 0, 0.2);
            font-size: 15px;
            overflow: hidden;
            width: 400px;
            position: relative;
        }

        ul.place_list {
            margin: 0;
            padding: 0;
            background: transparent;
            list-style: none;
        }

        ul.place_list li:last-child .suggest {
            border-radius: 0 0 2px 2px;
        }

        .suggest {
            position: relative;
            color: #8C8C8C;
            font-size: 12px;
            line-height: 24px;
            min-height: 24px;
            padding: 6px 6px 7px 0;
            border-top: 1px solid #e6e6e6;
            overflow: hidden;
            text-overflow: ellipsis;
            white-space: nowrap;
        }

        .suggest-icon-container {
            float: left;
            margin: 0 16px;
            background: no-repeat url(https://maps.gstatic.com/tactile/icons/suggest-7a1f96ff5ef6dd5f140f448ec355ab82.png) 0 -208px;
            height: 24px;
            width: 24px;
        }

        .suggest-query {
            color: #000;
            font-weight: 500;
        }

        #content {
            font-family: Arial, Microsoft JhengHei;
            max-width: 257px;
        }

        #bodyContent ul {
            padding-left: 18px;
            margin-bottom: 0;
        }

        #firstHeading {
            font-weight: bold;
        }
    </style>
@endsection

@section('content')
    <div class="container-fluid">

        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
                    <h4 class="page-title">Gerar Relatorio</h4>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0);">PRM</a></li>
                        <li class="breadcrumb-item"><a href="javascript:void(0);">Viaturas</a></li>
                        <li class="breadcrumb-item active">Gerar Relatorio</li>
                    </ol>
                </div>
            </div>
        </div>
        <!-- end row -->

        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <form action="" novalidate method="post">
                            @csrf
                            <div class="form-body">
                                <h4 class="card-title">Gerar Relatorio</h4>
                                <hr>
                                <div class="row">

                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Viatura</label>

                                            <select class="form-control custom-select" name="imei"
                                                    id="imei" required>
                                                <option value="">Selecione a Viatura</option>
                                                @foreach($vehicles as $vehicle)
                                                    <option
                                                        value="{{ $vehicle->imei }}">{{ $vehicle->number_plate }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Data e Hora de Inicio <span class="text-danger">*</span></label>
                                            <div class="controls">
                                                <input type="datetime-local" value="0" name="start_date" id="start_time"
                                                       class=" form-control" required></div>

                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Data e Hora de Fim <span class="text-danger">*</span></label>
                                            <div class="controls">
                                                <input type="datetime-local" value="0" name="end_date" id="end_time"
                                                       class=" form-control" required></div>

                                        </div>
                                    </div>


                                </div>
                            </div>
                            <hr>


                            <div class="row">
                                <div class="form-actions">
                                    <button type="button" onclick="getCarReport()" id="generateReport"
                                            class="btn btn-success">
                                        <i class="fa fa-check"></i> Gerar
                                    </button>

                                    <button type="button" onclick="removeLine()" id="generateReport"
                                            class="btn btn-danger">
                                        <i class="fa fa-eraser"></i> Limpar
                                    </button>
                                </div>
                            </div>

                        </form>

                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12" style="height: 400px">
                <div id="map"></div>
                <div id="directions-panel"></div>
            </div>
        </div>

        <div id="loader" class="lds-dual-ring hidden overlay"></div>
    </div>



@endsection

@section('script')
    <script>

        var responseData
        var geocoder;
        var map;
        var lat_lng = []
        var car_markers = []
        var markers = [];
        var content = ''
        var new_lat_lng = []
        var activeInfoWindow;
        var flightPath;

        var directionsService = new google.maps.DirectionsService();
        var directionsRenderer = new google.maps.DirectionsRenderer();

        function getWaypoints(waypoints, nr_waypoints) {
            var sizeWaypoints = waypoints.length;
            console.log("Waypoints size " + sizeWaypoints)
            var ofset = parseInt(sizeWaypoints / nr_waypoints);
            var new_waypoints = [];
            var i;
            var aux = 0;

            for (i = 0; i < nr_waypoints; i++) {
                new_waypoints.push(waypoints[aux])
                aux = aux + ofset;
                console.log(aux);
            }
            return new_waypoints;
        }

        function infoWidndowUpdate(index) {
           var viaura = $('select option:selected').text();
            return '<div class="card card-outline-info" style="margin-bottom: 0px;">\n' +
                '<div class="card-header" style="background: #ff931f; border-color: #ff931f; padding: .2rem 1.25rem;">\n' +
                '<h6 class="m-b-0 text-white"> Viatura ' + viaura + ' </h6>\n' +
                '</div>\n' +
                '<div class="card-body">\n' +
                '<span style="float: left">Data e Hora: </span>  <strong style="float: right">' + new_lat_lng[index].gprs_time + '</strong>\n' +
                '    <br>\n' +
                '                <br>\n' +
                '                    <span style="float: left">Velociade: </span>  <strong style="float: right">' + new_lat_lng[index].speed.toFixed(2) + ' Km/h</strong>\n' +
                '            </div>\n' +
                '        </div>\n'
        }

        function generateMarkers(locations) {

            var size = locations.length
            var icon;

            for (var i in locations) {
                var latitude = parseFloat(locations[i].latitude)
                var longitude = parseFloat(locations[i].longitude)

                var myLatLng = {lat: latitude, lng: longitude};
                // console.log(location)
                if (i == 0){
                     icon = { // car icon
                         url: "http://maps.google.com/mapfiles/ms/icons/green-dot.png"
                     };
                } else if (i== size-1) {
                     icon = { // car icon
                         url: "http://maps.google.com/mapfiles/ms/icons/yellow-dot.png"
                    };
                }else {
                    icon = { // car icon
                        path: google.maps.SymbolPath.FORWARD_CLOSED_ARROW,
                        scale: 4,
                        fillColor: "#00b5ff", //<-- Car Color, you can change it
                        fillOpacity: 1,
                        strokeWeight: 1,
                        anchor: new google.maps.Point(0, 5),
                        rotation: locations[i].direction//<-- Car angle
                    };
                }

                var marker = new google.maps.Marker({
                    position: myLatLng,
                    icon: icon,
                    map,
                    title: i,
                });

                var infowindow = new google.maps.InfoWindow()
                google.maps.event.addListener(marker, 'click', (function (marker, content, infowindow) {
                    return function () {
                        var index = parseInt(marker.getTitle())
                        content = infoWidndowUpdate(index)

                        if (activeInfoWindow) {
                            activeInfoWindow.close();
                        }

                        infowindow.setContent(content)
                        infowindow.open(map, marker);

                        activeInfoWindow = infowindow;

                        console.log(index)
                        setInterval(function () {
                            content = infoWidndowUpdate(index)
                            infowindow.setContent(content)
                        }, 2000);

                        setTimeout(function () {
                            $(".gm-style-iw-d").removeClass("gm-style-iw-d");
                        }, 100);

                    };
                })(marker, content, infowindow));

                markers.push(marker)
            }
            //Go to First Marker
            map.panTo(lat_lng[0]);
            map.setZoom( 12);

        }

        function removeLine() {
            if(flightPath != null){
                flightPath.setMap(null);
            }

            for (var i = 0; i < markers.length; i++) {
                console.log("Markers "+markers.length);
                markers[i].setMap(null);
            }
            markers = [];
            new_lat_lng = [];
            car_markers = [];
            lat_lng = [];
        }
        function getCarReport() {


           removeLine();

            var imei = $('#imei').val()
            var start = $('#start_time').val()
            var end = $('#end_time').val()
            var car_data = {
                imei: imei,
                start: start,
                end: end
            }
            $('#loader').removeClass('hidden')
            $.ajax({
                method: 'POST',
                url: "http://3.134.70.3/prm_dashboard/public/api/get-car-report",
                dataType: 'text',
                contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
                data: car_data,
                complete: function (data) {
                    console.log("success");
                },
                success: function (response) {

                    responseData = JSON.parse(response);
                    var position
                    var save = 0
                    for (var i in responseData) {
                        // position = {location: new google.maps.LatLng(parseFloat(responseData[i].latitude), parseFloat(responseData[i].longitude))}
                        var speed = responseData[i].speed

                        if (speed == 0 && speed<5){ // To avoid duplication of positions

                            if (save == 0){
                                position = {
                                    lat: parseFloat(responseData[i].latitude),
                                    lng: parseFloat(responseData[i].longitude)
                                }
                                var marker = {
                                    latitude: parseFloat(responseData[i].latitude),
                                    longitude: parseFloat(responseData[i].longitude),
                                    direction: responseData[i].direction,
                                    speed: responseData[i].speed,
                                    id: responseData[i].id,
                                    gprs_time: responseData[i].gprs_time,
                                    updated_at: responseData[i].updated_at,
                                    imei: responseData[i].imei,

                                }

                                car_markers.push(marker);
                                lat_lng.push(position);
                                save =1;
                            }
                        }else {
                            position = {
                                lat: parseFloat(responseData[i].latitude),
                                lng: parseFloat(responseData[i].longitude)
                            }
                            var marker = {
                                latitude: parseFloat(responseData[i].latitude),
                                longitude: parseFloat(responseData[i].longitude),
                                direction: responseData[i].direction,
                                speed: responseData[i].speed,
                                id: responseData[i].id,
                                gprs_time: responseData[i].gprs_time,
                                updated_at: responseData[i].updated_at,
                                imei: responseData[i].imei,

                            }

                            car_markers.push(marker);
                            lat_lng.push(position);

                            save =0;//To save again zero speed
                        }

                    }
                    if (lat_lng.length > 0) {

                        var pieces = 0;
                        if (car_markers.length <= 100){
                            pieces = car_markers.length;
                        } else{
                            pieces = 100;
                        }

                        new_lat_lng = getWaypoints(car_markers, pieces);
                        // lat_lng = getWaypoints(lat_lng, pieces);

                         flightPath = new google.maps.Polyline({
                            path: lat_lng,
                            geodesic: true,
                            strokeColor: "#ff0003",
                            strokeOpacity: 1.0,
                            strokeWeight: 5,
                        });
                        flightPath.setMap(map);

                        generateMarkers(new_lat_lng);
                        // calculateAndDisplayRoute(directionsService, directionsRenderer);
                        $('#loader').addClass('hidden')


                    } else {
                        alert('No data');
                        $('#loader').addClass('hidden')
                    }


                }, error: function (xhr, type, exception) {
                    console.log("Error " + xhr, type, exception + "Exception ");
                    //$('#email_error').css('display', 'block')
                    $('#loader').addClass('hidden')
                }
            });
        }


        function initMap() {
            directionsService = new google.maps.DirectionsService();
            directionsRenderer = new google.maps.DirectionsRenderer();
            var maputo = new google.maps.LatLng(-25.9596891, 32.5810377);
            var myOptions = {
                zoom: 6,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                center: maputo
            }
            map = new google.maps.Map(document.getElementById("map"), myOptions);
            directionsRenderer.setMap(map)

        }

        function calculateAndDisplayRoute(directionsService, directionsRenderer) {

            var last = responseData.length - 1
            directionsService.route(
                {
                    origin: new google.maps.LatLng(responseData[0].latitude, responseData[0].longitude),
                    destination: new google.maps.LatLng(responseData[last].latitude, responseData[last].longitude),
                    waypoints: new_lat_lng,
                    optimizeWaypoints: true,
                    travelMode: google.maps.TravelMode.DRIVING,
                },
                (response, status) => {
                    if (status === "OK") {
                        directionsRenderer.setDirections(response);
                        const route = response.routes[0];

                    } else {
                        window.alert("Directions request failed due to " + status);
                    }
                }
            );
        }

    </script>

    <!-- Parsley js -->
    <script src="{{ URL::asset('assets/plugins/parsleyjs/parsley.min.js')}}"></script>
@endsection

@section('script-bottom')
    <script>
        $(document).ready(function () {
            $('form').parsley();
        });
    </script>
@endsection
