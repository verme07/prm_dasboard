@extends('layouts.master')

@section('css')

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.3.0/socket.io.js"></script>
    <script src="https://polyfill.io/v3/polyfill.min.js?features=default"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAJloUT_O0qVDJHVr0pBkSrR2UV4GT_Yik&callback=initMap&libraries=&v=weekly"
            defer></script>
    <style>
        .gm-style .gm-style-iw-c {
            padding: 0px;
            overflow: unset;
        }
        #map {
            height: 100%}
        #planRouteMode {
            width: 78px;
            height: 24px;
            line-height: 24px;
            border: none;
            outline: none;
            float: right;
        }

        #getLocation {
            width: auto;
            height: 38px;
            padding: 5px;
            margin: 0;
            text-align: center;
            position: absolute;
            right: 355px;
            bottom: 120px;
        }
        .btn-getLocation {
            box-shadow: rgba(0, 0, 0, 0.3) 0px 1px 4px -1px;
            border-radius: 2px;
            cursor: pointer;
            background-color: #fff;
            width: 28px;
            height: 28px;
            right: 0;
            bottom: 5px;
        }
        .btn-getLocation i {
            position: relative;
            top: 4px;
            line-height: 20px;
            width: 20px;
            height: 20px;
            font-size: 18px;
            color: #666;
        }
        #myLocation {font-size: 12px; vertical-align: text-top;}
        @media (max-width: 992px) and (min-width: 520px) {
            #map {width: 100%; height: 500px;}
            #getLocation {top: 341px; right: 5px;}
        }
        @media (max-width: 520px) {
            #map {width: 100%; height: 200px;}
            #getLocation {top: 192px; bottom: 0; right: 5px;}
        }

        .badge-gray {background: #bdbdbd;}
        .badge-darkgreen {background: #00c853;}
        .badge-lightgreen {background: #64dd17;}
        .badge-yellow {background: #ffca00;}
        .badge-orange {background: #ff8300;}
        .badge-red {background: #f00;}
        .rating {
            width: 32px;
            height: 20px;
            line-height: 20px;
            padding: 1px;
        }
        .placeName {
            overflow: hidden;
            text-overflow: ellipsis;
            white-space: pre-line;
            height: 16px;
            width: 255px;
            display: block;
        }
        .map-list-items {
            overflow-y: scroll;
            width: 200px;
            position: relative;
            height: calc(100% - 70px);
            padding: 0;
            background-color: white;
        }
        .panel-success {
            position: absolute;
            right: 0;
            top: 0;
            bottom: 0;
            margin: 0;
            box-shadow: 0 0 20px rgba(0,0,0,0.3);
            border: none;
            margin-top: 70px;
        }
        .map-panel-title {
            background-color: #ea922d !important;
            color: #fff !important;
            border: none;
            padding: 11px 15px 9px 15px;
            width: 200px;
            height: 70px;
        }
        .map-panel-title h4 {margin: 6px 0;}
        .panel, .map-panel-title, .list-group-item:last-child, .list-group-item:first-child {
            border-radius: 0;
        }
        .list-group-item {
            height: 40px;
            margin-bottom: 0;
            border-left: none;
            border-right: none;
            border-top: none;
            border-bottom: 1px solid #ddd;
            cursor: pointer;
        }
        @media (max-width: 992px) and (min-width: 520px) {
            form .form-group {float: left;}
            form .form-group:nth-child(2) {margin-left: 10px;}
            .form-control {display: inline-block; width: auto;}
            .panel-success {
                position: static;
                right: inherit;
                top: inherit;
                bottom: inherit;
                height: calc(100% - 500px);
            }
            .map-panel-title {width: 100%;}
            .map-list-items {width: 100%; position: static;}
        }

        @media (max-width: 520px) {
            .panel-success {
                position: static;
                right: inherit;
                top: inherit;
                bottom: inherit;
                height: calc(100% - 200px);
            }
            .map-panel-title {width: 100%;}
            .map-list-items {width: 100%; position: static;}
        }

        #origin-input, #destination-input {
            background-color: #fff;
            font-family: Roboto;
            font-size: 15px;
            font-weight: 300;
            margin-left: 12px;
            padding: 0 11px 0 13px;
            text-overflow: ellipsis;
            width: 200px;
        }

        #origin-input:focus, #destination-input:focus {border-color: #4d90fe;}

        #mode-selector {
            color: #fff;
            background-color: #4d90fe;
            margin-left: 12px;
            padding: 5px 11px 0px 11px;
        }

        #mode-selector label {font-size: 13px;}

        input::-moz-placeholder{color:#999;opacity:1}
        input:-ms-input-placeholder{color:#999}
        input::-webkit-input-placeholder{color:#999}
        #omnibox {position: absolute; margin: 10px; z-index: 10; top: 0; margin-top: 75px}
        #searchbox_form {display: inline-block;}
        .searchbox.suggestions-shown {border-radius: 2px 2px 0 0;}
        .searchbox {
            position: relative;
            background: #fff;
            border-radius: 2px;
            box-sizing: border-box;
            width: 424px;
            height: 48px;
            border-bottom: 1px solid transparent;
            padding: 12px 2px 11px 16px;
            box-shadow: 0 2px 4px rgba(0,0,0,0.2), 0 -1px 0px rgba(0,0,0,0.02);
        }
        .searchbox #radius {
            border: none;
            outline: none;
            font-size: 15px;
            width: 56px;
            height: 24px;
            line-height: 24px;
        }
        .searchbox #keyword {
            border: none;
            padding: 0;
            outline: none;
            font-size: 15px;
            width: 210px;
            height: 24px;
            line-height: 24px;
        }
        .searchbox-searchbtn-container {position: absolute; right: 5px; top: 0;}
        .searchbox-searchbtn-container::before, .searchbox-searchbtn-container::after {
            content: "";
            position: absolute;
            top: 10px;
            border-left: 1px solid #ddd;
            height: 28px;
        }
        .searchbox-searchbtn-container::after {right: 0;}
        #searchbox-searchbtn {
            display: block;
            cursor: pointer;
            padding: 12px;
            margin: 0;
            border: 0;
            outline: 0;
            background: transparent;
            list-style: none;
        }
        #searchbox-searchbtn::before {
            content: '';
            display: block;
            width: 24px;
            height: 24px;
            background: url(https://maps.gstatic.com/tactile/omnibox/quantum_search_button-20150825-1x.png);
        }

        @media (max-width: 520px) {
            #omnibox {margin: 0 auto; width: 100%;}
            .searchbox {width: 100%; height: 40px; padding: 8px 4px 8px 8px;}
            #searchbox_form {width: calc(100% - 130px);}
            .searchbox #keyword {width: calc(100% - 60px);}
            #searchbox-searchbtn {padding: 8px 10px;}
            .searchbox-searchbtn-container {position: absolute; right: 88px; top: 0;}
            .searchbox-searchbtn-container::before, .searchbox-searchbtn-container::after {top: 6px;}
        }

        .place_suggestions {
            background-color: #fff;
            border-radius: 0 0 2px 2px;
            box-shadow: 0 2px 4px rgba(0,0,0,0.2);
            font-size: 15px;
            overflow: hidden;
            width: 400px;
            position: relative;
        }
        ul.place_list {
            margin: 0;
            padding: 0;
            background: transparent;
            list-style: none;
        }
        ul.place_list li:last-child .suggest {border-radius: 0 0 2px 2px;}
        .suggest {
            position: relative;
            color: #8C8C8C;
            font-size: 12px;
            line-height: 24px;
            min-height: 24px;
            padding: 6px 6px 7px 0;
            border-top: 1px solid #e6e6e6;
            overflow: hidden;
            text-overflow: ellipsis;
            white-space: nowrap;
        }
        .suggest-icon-container {
            float: left;
            margin: 0 16px;
            background: no-repeat url(https://maps.gstatic.com/tactile/icons/suggest-7a1f96ff5ef6dd5f140f448ec355ab82.png) 0 -208px;
            height: 24px;
            width: 24px;
        }
        .suggest-query {color: #000; font-weight: 500;}
        #content {font-family: Arial, Microsoft JhengHei; max-width: 257px;}
        #bodyContent ul {padding-left: 18px; margin-bottom: 0;}
        #firstHeading {font-weight: bold;}
    </style>
@endsection

@section('content')
    <div class="container-fluid" style="width: calc(100% - 185px); margin: 0px; padding: 0px">

        <div class="row">
            <div class="col-lg-12" style="height: 700px">
                <div id="map"></div>
            </div>
        </div>
    </div>


    <div class="panel panel-success">
        <div class="map-panel-title"><h4>Viaturas</h4> <span id="myLocation"></span></div>
        <div class="map-list-items">
            <div id="place-list" class="list-group" style="margin-bottom: 0;">
{{--                <li href="#" class="list-group-item" onClick="trackCar(0)">AHI - 575 - MC</li>--}}
{{--                <li href="#" class="list-group-item" onClick="trackCar(1)">AHI - 575 - MC</li>--}}
            </div>
        </div>


    </div>


@endsection

@section('script')
    <script type="text/javascript">
        var socket = io('http://3.134.70.3:3020');

        var locations = [];
        var locationsUpdate = [];
        var markers = [];
        var map;
        var activeInfoWindow;

        var content = ''

        function initMap() {
            const myLatLng = { lat:  -25.9596891, lng: 32.5810377};

            map = new google.maps.Map(document.getElementById("map"), {
                zoom: 14,
                center: myLatLng,
            });
        }

        function prettyDate(time){
            var date = new Date((time || "").replace(/-/g,"/").replace(/[TZ]/g," ")),
                diff = (((new Date()).getTime() - date.getTime()) / 1000),
                day_diff = Math.floor(diff / 86400);

            if ( isNaN(day_diff) || day_diff < 0 || day_diff >= 31 )
                return;

            return day_diff == 0 && (
                diff < 60 && "menos de 1 minuto" ||
                diff < 120 && "1 minuto atrás" ||
                diff < 3600 && Math.floor( diff / 60 ) + " minutos atrás" ||
                diff < 7200 && "1 hora atrás" ||
                diff < 86400 && Math.floor( diff / 3600 ) + " horas atrás") ||
                day_diff == 1 && "Ontem" ||
                day_diff < 7 && day_diff + " dias atrás" ||
                day_diff < 31 && Math.ceil( day_diff / 7 ) + " semanas atrás";
        }

        // If jQuery is included in the page, adds a jQuery plugin to handle it as well
        if ( typeof jQuery != "undefined" )
            jQuery.fn.prettyDate = function(){
                return this.each(function(){
                    var date = prettyDate(this.title);
                    if ( date )
                        jQuery(this).text( date );
                });
            };

        function trackCar(car_id) {

            console.log(car_id)
            var lati = markers[car_id].position.lat()
            var long = markers[car_id].position.lng()

            var latLng = new google.maps.LatLng(lati, long); //Makes a latlng
            map.panTo(latLng);
            map.setZoom(18)

            const infowindow = new google.maps.InfoWindow({
                content: infoWidndowUpdate(car_id),
            });
            if (activeInfoWindow ) { activeInfoWindow.close();}

            infowindow.setContent(content)
            infowindow.open(map, markers[car_id]);
            activeInfoWindow = infowindow;

            setInterval(function() {
                content = infoWidndowUpdate(car_id)
                infowindow.setContent(content)
            }, 2000  );

            setTimeout(function() {
                $( ".gm-style-iw-d" ).removeClass( "gm-style-iw-d" );
            }, 100  );

        }



        function animatedMove(marker, t, current, moveto, id) {

            var deltalat = (moveto.latitude - current.latitude) / 100;
            var deltalng = (moveto.longitude - current.longitude) / 100;

            var icon = { // car icon
                path: 'M29.395,0H17.636c-3.117,0-5.643,3.467-5.643,6.584v34.804c0,3.116,2.526,5.644,5.643,5.644h11.759   c3.116,0,5.644-2.527,5.644-5.644V6.584C35.037,3.467,32.511,0,29.395,0z M34.05,14.188v11.665l-2.729,0.351v-4.806L34.05,14.188z    M32.618,10.773c-1.016,3.9-2.219,8.51-2.219,8.51H16.631l-2.222-8.51C14.41,10.773,23.293,7.755,32.618,10.773z M15.741,21.713   v4.492l-2.73-0.349V14.502L15.741,21.713z M13.011,37.938V27.579l2.73,0.343v8.196L13.011,37.938z M14.568,40.882l2.218-3.336   h13.771l2.219,3.336H14.568z M31.321,35.805v-7.872l2.729-0.355v10.048L31.321,35.805',
                scale: 0.5,
                fillColor: "#ff931f", //<-- Car Color, you can change it
                fillOpacity: 1,
                strokeWeight: 1,
                anchor: new google.maps.Point(0, 5),
                rotation: moveto.direction//<-- Car angle
            };


            var delay = 10 * t;
            for (var i = 0; i < 100; i++) {
                (function(ind) {
                    setTimeout(
                        function() {
                            var lat = marker.position.lat();
                            var lng = marker.position.lng();
                            lat += deltalat;
                            lng += deltalng;
                            latlng = new google.maps.LatLng(lat, lng);

                            var dat = date = new Date(moveto.gprs_time);
                            dat.setHours(dat.getHours()+2)
                            var lastUpdate = prettyDate(dat.toISOString())

                            content = '<div class="card card-outline-info" style="margin-bottom: 0px;">\n' +
                                '<div class="card-header" style="background: #ff931f; border-color: #ff931f; padding: .2rem 1.25rem;">\n' +
                                '<h6 class="m-b-0 text-white"> Viatura '+moveto.number_plate+' </h6>\n' +
                                '</div>\n' +
                                '<div class="card-body">\n' +
                                '<span style="float: left">Ultimo Update: </span>  <strong style="float: right"> '+ lastUpdate+'</strong>\n' +
                                '<br>\n' +
                                '<br>\n' +
                                '<span style="float: left">Esquadra: </span>  <strong style="float: right">'+moveto.police_station_id+'</strong>\n' +
                                '                <br>\n' +
                                '                <br>\n' +
                                '                    <span style="float: left">Velociade: </span>  <strong style="float: right">'+moveto.speed.toFixed(2)+' Km/h</strong>\n' +
                                '                <br>\n' +
                                '                <br>\n' +
                                '                <span style="float: left">ID: </span>  <strong style="float: right">'+moveto.id+'</strong>\n' +
                                '            </div>\n' +
                                '        </div>\n'


                            marker.setPosition(latlng);

                        }, delay * ind
                    );
                })(i)
            }

            marker.setIcon(icon)
        }

        function infoWidndowUpdate(index) {

            var dat = date = new Date(locations[index].gprs_time);
            dat.setHours(dat.getHours()+2)
            var lastUpdate = prettyDate(dat.toISOString())

            return '<div class="card card-outline-info" style="margin-bottom: 0px;">\n' +
                '<div class="card-header" style="background: #ff931f; border-color: #ff931f; padding: .2rem 1.25rem;">\n' +
                '<h6 class="m-b-0 text-white"> Viatura '+locations[index].number_plate+' </h6>\n' +
                '</div>\n' +
                '<div class="card-body">\n' +
                '<span style="float: left">Ultimo Update: </span>  <strong style="float: right">'+lastUpdate+'</strong>\n' +
                '<br>\n' +
                '<br>\n' +
                '<span style="float: left">Esquadra: </span>  <strong style="float: right">'+locations[index].police_station_id+'</strong>\n' +
                '                <br>\n' +
                '                <br>\n' +
                '                    <span style="float: left">Velociade: </span>  <strong style="float: right">'+locations[index].speed.toFixed(2)+' Km/h</strong>\n' +
                '                <br>\n' +
                '                <br>\n' +
                '                <span style="float: left">ID: </span>  <strong style="float: right">'+locations[index].id+'</strong>\n' +
                '            </div>\n' +
                '        </div>\n'
        }


        function carsUpdate(carData){
            // locations = JSON.parse(carData)

            if (markers.length>0){
                locationsUpdate = JSON.parse(carData);

                for(var i in locations){

                    animatedMove(markers[i], 3, locations[i], locationsUpdate[i], i)
                }

                locations = JSON.parse(carData);

            }else {
                locations = JSON.parse(carData)

                console.log(locations)
                for(var i in locations){
                    var latitude = parseFloat(locations[i].latitude)
                    var longitude = parseFloat(locations[i].longitude)

                    var myLatLng = { lat: latitude, lng: longitude };
                    // console.log(location)
                    var icon = { // car icon
                        path: 'M29.395,0H17.636c-3.117,0-5.643,3.467-5.643,6.584v34.804c0,3.116,2.526,5.644,5.643,5.644h11.759   c3.116,0,5.644-2.527,5.644-5.644V6.584C35.037,3.467,32.511,0,29.395,0z M34.05,14.188v11.665l-2.729,0.351v-4.806L34.05,14.188z    M32.618,10.773c-1.016,3.9-2.219,8.51-2.219,8.51H16.631l-2.222-8.51C14.41,10.773,23.293,7.755,32.618,10.773z M15.741,21.713   v4.492l-2.73-0.349V14.502L15.741,21.713z M13.011,37.938V27.579l2.73,0.343v8.196L13.011,37.938z M14.568,40.882l2.218-3.336   h13.771l2.219,3.336H14.568z M31.321,35.805v-7.872l2.729-0.355v10.048L31.321,35.805',
                        scale: 0.5,
                        fillColor: "#ff931f", //<-- Car Color, you can change it
                        fillOpacity: 1,
                        strokeWeight: 1,
                        anchor: new google.maps.Point(0, 5),
                        rotation: locations[i].direction//<-- Car angle
                    };
                    var marker = new google.maps.Marker({
                        position: myLatLng,
                        icon: icon,
                        map,
                        title: i,
                    });


                    var infowindow = new google.maps.InfoWindow()
                    google.maps.event.addListener(marker,'click', (function(marker,content,infowindow){
                        return function() {
                            var index = parseInt(marker.getTitle())
                            content = infoWidndowUpdate(index)

                            if (activeInfoWindow ) { activeInfoWindow.close();}

                            infowindow.setContent(content)
                            infowindow.open(map,marker);

                            activeInfoWindow = infowindow;

                            console.log(index)
                            setInterval(function() {
                                content = infoWidndowUpdate(index)
                                infowindow.setContent(content)
                            }, 2000  );

                            setTimeout(function() {
                                $( ".gm-style-iw-d" ).removeClass( "gm-style-iw-d" );
                            }, 100  );

                        };
                    })(marker,content,infowindow));

                    markers.push(marker)

                }
                createList(locations)
            }


        }

        function createList(locations){
            for(var i in locations){
                var item = '<li href="#" class="list-group-item" onClick="trackCar('+i+')">'+locations[i].number_plate+'</li>'
                $("#place-list").append(item);
            }
        }

        setInterval(function(){
            var messageObject = {
                channel_id: 1
            };
            socket.emit('retrieveAllCars', messageObject);
        }, 1600);

        socket.on('channel_1', function (message) {
            // renderMessage(message)
            carsUpdate(message)

        });

    </script>
    <!-- Parsley js -->
    <script src="{{ URL::asset('assets/plugins/parsleyjs/parsley.min.js')}}"></script>
@endsection

@section('script-bottom')
    <script>
        $(document).ready(function () {
            $('form').parsley();
        });
    </script>
@endsection
