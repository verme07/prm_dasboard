@extends('layouts.master')

@section('css')

@endsection

@section('content')
    <div class="container-fluid">

        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
                    <h4 class="page-title">Adicionar Viatura</h4>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0);">PRM</a></li>
                        <li class="breadcrumb-item"><a href="javascript:void(0);">Viaturas</a></li>
                        <li class="breadcrumb-item active">Adicionar Viatura</li>
                    </ol>
                </div>
            </div>
        </div>
        <!-- end row -->

        <div class="row">
            <div class="col-lg-12">
                <div class="card m-b-20">
                    <div class="card-body">

                        <h4 class="mt-0 header-title">Adicionar Viatura</h4>

                        <form class="" method="POST" enctype="multipart/form-data" action="{{ route('save-vehicle') }}">
                            @csrf
                            <div class="row">
                                <div class="col-6">
                                    <div class="form-group">
                                        <label>IMEI <span>*</span></label>
                                        <input name="imei" id="imei " type="text" class="form-control" required/>
                                    </div>
                                    <div class="form-group">
                                        <label>Marca <span>*</span></label>
                                        <input name="brand" id="brand " type="text" class="form-control" required/>
                                    </div>
                                    <div class="form-group">
                                        <label>Modelo <span>*</span></label>
                                        <input name="reference" id="reference " type="text" class="form-control" required/>
                                    </div>
                                    <div class="form-group">
                                        <label>Lotação <span>*</span></label>
                                        <input name="space" id="space " type="text" class="form-control" required/>
                                    </div>

                                </div>

                                <div class="col-6">
                                    <div class="form-group">
                                        <label>Posto Policial </label>
                                        <select class="form-control" name="police_station_id" id="police_station_id" required="required">
                                            <option value="">Selecione o posto policial ao qual pertence a viatura</option>
                                            @foreach($polices_stations as $police_station)
                                                <option value="{{ $police_station->id }}">{{ $police_station->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Numero do Motor</label>
                                        <input name="engine_nr" id="engine_nr " type="text" class="form-control" required/>
                                    </div>

                                    <div class="form-group">
                                        <label>Matricula <span>*</span></label>
                                        <input name="number_plate" id="number_plate " type="text" class="form-control" required/>
                                    </div>
                                    <div class="form-group">
                                        <label>Combustivel </label>
                                        <select class="form-control" name="gas_type" id="gas_type" required>
                                            <option value="">Selecione o tipo de Combustivel</option>
                                            <option value="Gasoleo">Gasoleo</option>
                                            <option value="Gasolina">Gasolina</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div>
                                    <button type="submit" class="btn btn-primary waves-effect waves-light">
                                        Adicionar
                                    </button>
                                    <button type="reset" class="btn btn-secondary waves-effect m-l-5">
                                        Cancelar
                                    </button>
                                </div>
                            </div>
                        </form>

                    </div>
                </div>
            </div> <!-- end col -->

        </div> <!-- end row -->

    </div> <!-- container-fluid -->
@endsection

@section('script')
    <!-- Parsley js -->
    <script src="{{ URL::asset('assets/plugins/parsleyjs/parsley.min.js')}}"></script>
@endsection

@section('script-bottom')
    <script>
        $(document).ready(function() {
            $('form').parsley();
        });
    </script>
@endsection
