@extends('layouts.master')

@section('css')
    <!-- DataTables -->
    <link href="{{ asset('assets/plugins/datatables/dataTables.bootstrap4.min.css')}}" rel="stylesheet"
          type="text/css"/>
    <link href="{{ asset('assets/plugins/datatables/buttons.bootstrap4.min.css')}}" rel="stylesheet"
          type="text/css"/>
    <!-- Responsive datatable examples -->
    <link href="{{ asset('assets/plugins/datatables/responsive.bootstrap4.min.css')}}" rel="stylesheet"
          type="text/css"/>

    <link href="{{ asset('assets/plugins/ion-rangeslider/ion.rangeSlider.skinModern.css')}}" rel="stylesheet"
          type="text/css"/>
@endsection

@section('content')
    <div class="container-fluid">

        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
                    <h4 class="page-title">Adicionar Documento</h4>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0);">PRM</a></li>
                        <li class="breadcrumb-item"><a href="javascript:void(0);">Documento</a></li>
                        <li class="breadcrumb-item active">Adicionar documento</li>
                    </ol>
                </div>
            </div>
        </div>
        <!-- end row -->
        <div class="row">

            <div class="col-xl-3 col-md-3">
                <a class="card mini-stat bg-primary" href="{{ route('add-doc-auto-denuncia') }}">
                    <div class="card-body mini-stat-img">
                        <div class="mini-stat-icon">
                            <i class="fas fa-file-word float-right"></i>
                        </div>
                        <div class="text-white">
                            <h6 class="text-uppercase mb-3">Auto de Denuncia</h6>
                        </div>
                    </div>
                </a>
            </div>
        </div>
    </div> <!-- container-fluid -->
@endsection

@section('script')
    <script>
        function removerUser(doc_id) {

            $('#removerModal').modal({backdrop: 'static', keyboard: false})
                .on('click', '#delete-btn', function () {
                    let user = {
                        _token: "{{ csrf_token() }}",
                        doc_id: doc_id,
                    }
                    $.ajax({
                        method: 'POST',
                        url: "{{ route('remove-auto-denuncia') }}",
                        dataType: 'text',
                        contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
                        data: user,
                        complete: function (data) {
                            console.log("success");
                        },
                        success: function (response) {

                            let resposta = JSON.parse(response)
                            console.log(resposta)
                            if (resposta.status == 1) {
                                location.reload();
                            } else {
                                console.log("Erro")
                            }
                            ;

                        }, error: function (xhr, type, exception) {
                            console.log("Error " + xhr, type, exception + "Exception ");
                            //$('#email_error').css('display', 'block')

                        }
                    });
                })
        }
    </script>
    <!-- Required datatable js -->
    <script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js')}}"></script>
    <script src="{{ asset('assets/plugins/datatables/dataTables.bootstrap4.min.js')}}"></script>
    <!-- Buttons examples -->
    <script src="{{ asset('assets/plugins/datatables/dataTables.buttons.min.js')}}"></script>
    <script src="{{ asset('assets/plugins/datatables/buttons.bootstrap4.min.js')}}"></script>
    <script src="{{ asset('assets/plugins/datatables/jszip.min.js')}}"></script>
    <script src="{{ asset('assets/plugins/datatables/pdfmake.min.js')}}"></script>
    <script src="{{ asset('assets/plugins/datatables/vfs_fonts.js')}}"></script>
    <script src="{{ asset('assets/plugins/datatables/buttons.html5.min.js')}}"></script>
    <script src="{{ asset('assets/plugins/datatables/buttons.print.min.js')}}"></script>
    <script src="{{ asset('assets/plugins/datatables/buttons.colVis.min.js')}}"></script>
    <!-- Responsive examples -->
    <script src="{{ asset('assets/plugins/datatables/dataTables.responsive.min.js')}}"></script>
    <script src="{{ asset('assets/plugins/datatables/responsive.bootstrap4.min.js')}}"></script>

    <!-- Datatable init js -->
    <script src="{{ asset('assets/pages/datatables.init.js')}}"></script>
@endsection
