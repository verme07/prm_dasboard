@extends('layouts.master')

@section('css')

@endsection

@section('content')
    <div class="container-fluid">

        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
                    <h4 class="page-title">Adicionar Auto de Denuncia</h4>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0);">PRM</a></li>
                        <li class="breadcrumb-item"><a href="javascript:void(0);">Documentos</a></li>
                        <li class="breadcrumb-item"><a href="javascript:void(0);">Gerar Documento</a></li>
                        <li class="breadcrumb-item active">Adicionar Auto de Denuncia</li>
                    </ol>
                </div>
            </div>
        </div>
        <!-- end row -->

        <div class="row">
            <div class="col-lg-12">
                <div class="card m-b-20">
                    <div class="card-body">

                        <h4 class="mt-0 header-title">Adicionar Auto de Denuncia</h4>

                        <form class="" method="POST" enctype="multipart/form-data" action="{{ route('save-auto-denuncia') }}">
                            @csrf
                            <div class="row">
                                <div class="col-6">
                                    <div class="form-group">
                                        <label>Auto de Denuncia Nr. <span>*</span></label>
                                        <input name="complaint_nr" id="complaint_nr " type="text" class="form-control" required/>
                                    </div>

                                    <div class="form-group">
                                        <label>Esquadra Nr. <span>*</span></label>
                                        <input name="squad_nr" id="squad_nr " type="text" class="form-control" required/>
                                    </div>
                                    <div class="form-group">
                                        <label>Provincia </label>
                                        <select class="form-control" name="province" id="province" required>
                                            <option value="">Selecione a provincia</option>
                                            <option value="Maputo">Maputo</option>
                                            <option value="Gaza">Gaza</option>
                                            <option value="Inhambane">Inhambane</option>

                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label>Zona <span>*</span></label>
                                        <input name="zone" id="zone" type="text" class="form-control" required/>
                                    </div>

                                    <div class="form-group">
                                        <label>Dia <span>*</span></label>
                                        <input name="day" id="day" type="number" min="1" max="31" class="form-control" required/>
                                    </div>
                                    <div class="form-group">
                                        <label>Mês <span>*</span></label>
                                        <input name="month" id="month" type="number" min="1" max="12" class="form-control" required/>
                                    </div>
                                    <div class="form-group">
                                        <label>Ano <span>*</span></label>
                                        <input name="year" id="year" type="number" min="1900"  class="form-control" required/>
                                    </div>

                                    <div class="form-group">
                                        <label>Hora <span>*</span></label>
                                        <input name="time" id="time " type="text" class="form-control" required/>
                                    </div>
                                    <div class="form-group">
                                        <label>Nome do Cidadão <span>*</span></label>
                                        <input name="name" id="name " type="text" class="form-control" required/>
                                    </div>

                                    <div class="form-group">
                                        <label>Estado Civil </label>
                                        <select class="form-control" name="civil_state" id="civil_state" required>
                                            <option value="">Selecione o Estado Civil</option>
                                            <option value="Solteiro">Solteiro</option>
                                            <option value="Casado">Casado</option>
                                            <option value="Divorciado">Divorciado</option>
                                            <option value="Viúvo">Viúvo</option>

                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Genero </label>
                                        <select class="form-control" name="gender" id="gender" required>
                                            <option value="">Selecione o Genero</option>
                                            <option value="Masculino">Masculino</option>
                                            <option value="Feminino">Feminino</option>
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label>Idade <span>*</span></label>
                                        <input name="age" id="age" type="number" class="form-control" required/>
                                    </div>

                                    <div class="form-group">
                                        <label>Dia de Nascimento <span>*</span></label>
                                        <input name="day_birth" id="day_birth" type="number" min="1" max="31" class="form-control" required/>
                                    </div>
                                    <div class="form-group">
                                        <label>Mes de Nascimento </label>
                                        <select class="form-control" name="month_birth" id="month_birth" required>
                                            <option value="">Selecione o Mês</option>
                                            <option value="Janeiro">Janeiro</option>
                                            <option value="Fevereiro">Fevereiro</option>
                                            <option value="Março">Março</option>
                                            <option value="Abril">Abril</option>
                                            <option value="Maio">Maio</option>
                                            <option value="Junho">Junho</option>
                                            <option value="Julho">Julho</option>
                                            <option value="Agosto">Agosto</option>
                                            <option value="Setembro">Setembro</option>
                                            <option value="Outubro">Outubro</option>
                                            <option value="Novembro">Novembro</option>
                                            <option value="Dezembro">Dezembro</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Ano de Nascimento <span>*</span></label>
                                        <input name="year_birth" id="year_birth" type="number" min="1900"  class="form-control" required/>
                                    </div>
                                    <div class="form-group">
                                        <label>Nome do Pai <span>*</span></label>
                                        <input name="name_father" id="name_father" type="text"  class="form-control" required/>
                                    </div>
                                    <div class="form-group">
                                        <label>Nome da Mãe <span>*</span></label>
                                        <input name="name_mother" id="name_mother" type="text"  class="form-control" required/>
                                    </div>

                                </div>

                                <div class="col-6">

                                    <div class="form-group">
                                        <label>Natural de <span>*</span></label>
                                        <input name="natural" id="natural" type="text"  class="form-control" required/>
                                    </div>

                                    <div class="form-group">
                                        <label>Distrito <span>*</span></label>
                                        <input name="district" id="district" type="text"  class="form-control" required/>
                                    </div>
                                    <div class="form-group">
                                        <label>Natural da Provincia  </label>
                                        <select class="form-control" name="natural_province" id="natural_province" required>
                                            <option value="">Selecione a provincia</option>
                                            <option value="Maputo">Maputo</option>
                                            <option value="Gaza">Gaza</option>
                                            <option value="Inhambane">Inhambane</option>

                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Nacionalidade <span>*</span></label>
                                        <input name="nationality" id="nationality" type="text"  class="form-control" required/>
                                    </div>
                                    <div class="form-group">
                                        <label>Profissão <span>*</span></label>
                                        <input name="profession" id="profession" type="text"  class="form-control" required/>
                                    </div>
                                    <div class="form-group">
                                        <label>Residência <span>*</span></label>
                                        <input name="residence" id="residence" type="text"  class="form-control" required/>
                                    </div>
                                    <div class="form-group">
                                        <label>Denuncia na qualidade de <span>*</span></label>
                                        <input name="complaint_quality" id="complaint_quality" type="text"  class="form-control" required/>
                                    </div>

                                    <div class="form-group">
                                        <label>Dia do Caso <span>*</span></label>
                                        <input name="case_day" id="case_day" type="number" min="1" max="31" class="form-control" required/>
                                    </div>
                                    <div class="form-group">
                                        <label>Mês do Caso<span>*</span></label>
                                        <input name="case_month" id="case_month" type="number" min="1" max="12" class="form-control" required/>
                                    </div>
                                    <div class="form-group">
                                        <label>Ano do Caso<span>*</span></label>
                                        <input name="case_year" id="case_year" type="number" min="1900"  class="form-control" required/>
                                    </div>

                                    <div class="form-group">
                                        <label>Hora do Caso<span>*</span></label>
                                        <input name="case_time" id="case_time " type="text" class="form-control" required/>
                                    </div>

                                    <div class="form-group">
                                        <label>Local do Caso<span>*</span></label>
                                        <input name="case_place" id="case_place " type="text" class="form-control" required/>
                                    </div>
                                    <div class="form-group">
                                        <label>Endereço do Caso<span>*</span></label>
                                        <input name="case_address" id="case_address " type="text" class="form-control" required/>
                                    </div>
                                    <div class="form-group">
                                        <label>Descrição do Caso<span class="text-danger">*</span></label>
                                        <div class="controls">

                                            <textarea required="" name="case_description" class="form-control" rows="5"></textarea>
                                        </div>
                                    </div>


                                </div>
                            </div>
                            <div class="form-group">
                                <div>
                                    <button type="submit" class="btn btn-primary waves-effect waves-light">
                                        Adicionar
                                    </button>
                                    <button type="reset" class="btn btn-secondary waves-effect m-l-5">
                                        Cancelar
                                    </button>
                                </div>
                            </div>
                        </form>

                    </div>
                </div>
            </div> <!-- end col -->

        </div> <!-- end row -->

    </div> <!-- container-fluid -->
@endsection

@section('script')
    <!-- Parsley js -->
    <script src="{{ URL::asset('assets/plugins/parsleyjs/parsley.min.js')}}"></script>
@endsection

@section('script-bottom')
    <script>
        $(document).ready(function() {
            $('form').parsley();
        });
    </script>
@endsection
