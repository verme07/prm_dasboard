@extends('layouts.master')

@section('css')

@endsection

@section('content')
    <div class="container-fluid">

        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
                    <h4 class="page-title">Duplicar Auto de Denuncia</h4>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0);">PRM</a></li>
                        <li class="breadcrumb-item"><a href="javascript:void(0);">Documentos</a></li>
                        <li class="breadcrumb-item"><a href="javascript:void(0);">Lista de Documento</a></li>
                        <li class="breadcrumb-item active">Duplicar Auto de Denuncia</li>
                    </ol>
                </div>
            </div>
        </div>
        <!-- end row -->

        <div class="row">
            <div class="col-lg-12">
                <div class="card m-b-20">
                    <div class="card-body">

                        <h4 class="mt-0 header-title">Duplicar Auto de Denuncia</h4>

                        <form class="" method="POST" enctype="multipart/form-data" action="{{ route('save-auto-denuncia') }}">
                            @csrf
                            <div class="row">
                                <div class="col-6">
                                    <div class="form-group">
                                        <label>Auto de Denuncia Nr. <span>*</span></label>
                                        <input name="doc_id" value="{{ $document->id }}" type="hidden" class="form-control" required/>
                                        <input name="complaint_nr" value="{{ $document->complaint_nr }}" id="complaint_nr " type="text" class="form-control" required/>
                                    </div>

                                    <div class="form-group">
                                        <label>Esquadra Nr. <span>*</span></label>
                                        <input name="squad_nr" id="squad_nr" value="{{ $document->squad_nr }}" type="text" class="form-control" required/>
                                    </div>
                                    <div class="form-group">
                                        <label>Provincia </label>
                                        <select class="form-control" name="province" id="province" required>
                                            <option value="">Selecione a provincia</option>
                                            <option value="Maputo" {{ $document->province == "Maputo" ? 'selected="selected"' : '' }}>Maputo</option>
                                            <option value="Gaza" {{ $document->province == "Gaza" ? 'selected="selected"' : '' }}>Gaza</option>
                                            <option value="Inhambane" {{ $document->province == "Inhambane" ? 'selected="selected"' : '' }}>Inhambane</option>

                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label>Zona <span>*</span></label>
                                        <input name="zone" value="{{ $document->zone }}" id="zone" type="text" class="form-control" required/>
                                    </div>

                                    <div class="form-group">
                                        <label>Dia <span>*</span></label>
                                        <input name="day" id="day" type="number" value="{{ $document->day }}" min="1" max="31" class="form-control" required/>
                                    </div>
                                    <div class="form-group">
                                        <label>Mês <span>*</span></label>
                                        <input name="month" id="month" type="number" value="{{ $document->month }}" min="1" max="12" class="form-control" required/>
                                    </div>
                                    <div class="form-group">
                                        <label>Ano <span>*</span></label>
                                        <input name="year" id="year" type="number" value="{{ $document->year }}"  min="1900"  class="form-control" required/>
                                    </div>

                                    <div class="form-group">
                                        <label>Hora <span>*</span></label>
                                        <input name="time" id="time" value="{{ $document->time }}" type="text" class="form-control" required/>
                                    </div>
                                    <div class="form-group">
                                        <label>Nome do Cidadão <span>*</span></label>
                                        <input name="name" id="name" value="{{ $document->name }}" type="text" class="form-control" required/>
                                    </div>

                                    <div class="form-group">
                                        <label>Estado Civil </label>
                                        <select class="form-control" name="civil_state" id="civil_state" required>
                                            <option value="">Selecione o Estado Civil</option>
                                            <option value="Solteiro" {{ $document->civil_state == "Solteiro" ? 'selected="selected"' : '' }}>Solteiro</option>
                                            <option value="Casado" {{ $document->civil_state == "Casado" ? 'selected="selected"' : '' }}>Casado</option>
                                            <option value="Divorciado" {{ $document->civil_state == "Divorciado" ? 'selected="selected"' : '' }}>Divorciado</option>
                                            <option value="Viúvo" {{ $document->civil_state == "Viúvo" ? 'selected="selected"' : '' }}>Viúvo</option>

                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Genero </label>
                                        <select class="form-control" name="gender" id="gender" required>
                                            <option value="">Selecione o Genero</option>
                                            <option value="Masculino" {{ $document->gender == "Masculino" ? 'selected="selected"' : '' }}>Masculino</option>
                                            <option value="Feminino" {{ $document->gender == "Feminino" ? 'selected="selected"' : '' }}>Feminino</option>
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label>Idade <span>*</span></label>
                                        <input name="age" id="age" value="{{ $document->age }}" type="number" class="form-control" required/>
                                    </div>

                                    <div class="form-group">
                                        <label>Dia de Nascimento <span>*</span></label>
                                        <input name="day_birth" id="day_birth" value="{{ $document->day_birth }}" type="number" min="1" max="31" class="form-control" required/>
                                    </div>
                                    <div class="form-group">
                                        <label>Mes de Nascimento </label>
                                        <select class="form-control" name="month_birth" id="month_birth" required>
                                            <option value="">Selecione o Mês</option>
                                            <option value="Janeiro" {{ $document->month_birth == "Janeiro" ? 'selected="selected"' : '' }}>Janeiro</option>
                                            <option value="Fevereiro" {{ $document->month_birth == "Fevereiro" ? 'selected="selected"' : '' }}>Fevereiro</option>
                                            <option value="Março" {{ $document->month_birth == "Março" ? 'selected="selected"' : '' }}>Março</option>
                                            <option value="Abril" {{ $document->month_birth == "Abril" ? 'selected="selected"' : '' }}>Abril</option>
                                            <option value="Maio" {{ $document->month_birth == "Maio" ? 'selected="selected"' : '' }}>Maio</option>
                                            <option value="Junho" {{ $document->month_birth == "Junho" ? 'selected="selected"' : '' }}>Junho</option>
                                            <option value="Julho" {{ $document->month_birth == "Julho" ? 'selected="selected"' : '' }}>Julho</option>
                                            <option value="Agosto" {{ $document->month_birth == "Agosto" ? 'selected="selected"' : '' }}>Agosto</option>
                                            <option value="Setembro" {{ $document->month_birth == "Setembro" ? 'selected="selected"' : '' }}>Setembro</option>
                                            <option value="Outubro" {{ $document->month_birth == "Outubro" ? 'selected="selected"' : '' }}>Outubro</option>
                                            <option value="Novembro" {{ $document->month_birth == "Novembro" ? 'selected="selected"' : '' }}>Novembro</option>
                                            <option value="Dezembro" {{ $document->month_birth == "Dezembro" ? 'selected="selected"' : '' }}>Dezembro</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Ano de Nascimento <span>*</span></label>
                                        <input name="year_birth" id="year_birth" value="{{ $document->year_birth }}" type="number" min="1900"  class="form-control" required/>
                                    </div>
                                    <div class="form-group">
                                        <label>Nome do Pai <span>*</span></label>
                                        <input name="name_father" id="name_father" value="{{ $document->name_father }}" type="text"  class="form-control" required/>
                                    </div>
                                    <div class="form-group">
                                        <label>Nome da Mãe <span>*</span></label>
                                        <input name="name_mother" id="name_mother" value="{{ $document->name_mother }}" type="text"  class="form-control" />
                                    </div>

                                </div>

                                <div class="col-6">

                                    <div class="form-group">
                                        <label>Natural de <span>*</span></label>
                                        <input name="natural" id="natural" value="{{ $document->natural }}" type="text"  class="form-control" required/>
                                    </div>

                                    <div class="form-group">
                                        <label>Distrito <span>*</span></label>
                                        <input name="district" id="district" value="{{ $document->district }}" type="text"  class="form-control" required/>
                                    </div>
                                    <div class="form-group">
                                        <label>Natural da Provincia  </label>
                                        <select class="form-control" name="natural_province" id="natural_province" required>
                                            <option value="">Selecione a provincia</option>
                                            <option value="Maputo" {{ $document->natural_province == "Dezembro" ? 'selected="selected"' : '' }}>Maputo</option>
                                            <option value="Gaza" {{ $document->natural_province == "Gaza" ? 'selected="selected"' : '' }}>Gaza</option>
                                            <option value="Inhambane" {{ $document->natural_province == "Inhambane" ? 'selected="selected"' : '' }}>Inhambane</option>

                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Nacionalidade <span>*</span></label>
                                        <input name="nationality" id="nationality" value="{{ $document->nationality }}" type="text"  class="form-control" required/>
                                    </div>
                                    <div class="form-group">
                                        <label>Profissão <span>*</span></label>
                                        <input name="profession" id="profession" type="text" value="{{ $document->profession }}" class="form-control" required/>
                                    </div>
                                    <div class="form-group">
                                        <label>Residência <span>*</span></label>
                                        <input name="residence" id="residence" type="text" value="{{ $document->residence }}"  class="form-control" required/>
                                    </div>
                                    <div class="form-group">
                                        <label>Denuncia na qualidade de <span>*</span></label>
                                        <input name="complaint_quality" id="complaint_quality" value="{{ $document->complaint_quality }}" type="text"  class="form-control" required/>
                                    </div>

                                    <div class="form-group">
                                        <label>Dia do Caso <span>*</span></label>
                                        <input name="case_day" id="case_day" type="number" value="{{ $document->case_day }}" min="1" max="31" class="form-control" required/>
                                    </div>
                                    <div class="form-group">
                                        <label>Mês do Caso<span>*</span></label>
                                        <input name="case_month" id="case_month" type="number" value="{{ $document->case_month }}" min="1" max="12" class="form-control" required/>
                                    </div>
                                    <div class="form-group">
                                        <label>Ano do Caso<span>*</span></label>
                                        <input name="case_year" id="case_year" type="number" value="{{ $document->case_year }}" min="1900"  class="form-control" required/>
                                    </div>

                                    <div class="form-group">
                                        <label>Hora do Caso<span>*</span></label>
                                        <input name="case_time" id="case_time" value="{{ $document->case_time }}" type="text" class="form-control" required/>
                                    </div>

                                    <div class="form-group">
                                        <label>Local do Caso<span>*</span></label>
                                        <input name="case_place" id="case_place" value="{{ $document->case_place }}" type="text" class="form-control" required/>
                                    </div>
                                    <div class="form-group">
                                        <label>Endereço do Caso<span>*</span></label>
                                        <input name="case_address" id="case_address" value="{{ $document->case_address }}" type="text" class="form-control" required/>
                                    </div>
                                    <div class="form-group">
                                        <label>Descrição do Caso<span class="text-danger">*</span></label>
                                        <div class="controls">

                                            <textarea required="" name="case_description" class="form-control" rows="5">{{ $document->case_description }}</textarea>
                                        </div>
                                    </div>


                                </div>
                            </div>
                            <div class="form-group">
                                <div>
                                    <button type="submit" class="btn btn-primary waves-effect waves-light">
                                        Adicionar
                                    </button>
                                </div>
                            </div>
                        </form>

                    </div>
                </div>
            </div> <!-- end col -->

        </div> <!-- end row -->

    </div> <!-- container-fluid -->
@endsection

@section('script')
    <!-- Parsley js -->
    <script src="{{ URL::asset('assets/plugins/parsleyjs/parsley.min.js')}}"></script>
@endsection

@section('script-bottom')
    <script>
        $(document).ready(function() {
            $('form').parsley();
        });
    </script>
@endsection
