<p align="center">
    <img
        width="61"
        height="60"
        src="{{public_path('assets/images/test.png')}}"
    />
</p>
<p align="center">
    REPÚBLICA DE MOÇAMBIQUE
</p>
<p align="center">
    MINISTÉRIO DO INTERIOR
</p>
<p align="center">
    COMANDO DA PRM – CIDADE DE MAPUTO
</p>
<br>
<br>
<p>
    Auto de denúncia n° <u>{{ $document->complaint_nr }}</u><strong> PPMG ‘B”/2020 </strong>Unidade, 24ª
    Esquadra <u>{{ $document->squad_nr }}</u>, Distrito Municipal Kambukwana, Província <u>{{ $document->province }}</u>,
    Zona
    <u>{{ $document->zone }}</u>, Data
    <u>{{ $document->day }}</u>/<u>{{ $document->month }}</u>/<u>{{ $document->year }}</u>, Horas
    <u>{{ $document->time }}</u>
</p>
<br>
<p align="center">
    __________________________________________
</p>
<p align="center">
    (Classificação do Crime)
</p>
<br>

<p>
    Nesta subunidade apresentou-se o cidadão:
    <u>{{ $document->citizen_name }}</u> Estado Civil <u>{{ $document->civil_state }}</u>,
    Sexo <u>{{ $document->gender }}</u>, de <u>{{ $document->age }}</u> anos de idade, nascido (a) aos
    <u>{{ $document->day_birth }}</u> , do mês
    de <u>{{ $document->month_birth }}</u> de <u>{{ $document->year_birth }}</u>, filho de
    <u>{{ $document->name_father }}, e de <u>{{ $document->name_mother }}</u></u>
    , Natural de <u>{{ $document->natural }}</u>, Distrito de <u>{{ $document->district }}</u>, Província de
    <u>{{ $document->natural_province }}</u> , Nacionalidade <u>{{ $document->nationality }}</u>, Profissão
    <u>{{ $document->profession }}</u>, Residente <u>{{ $document->residence }}</u>, Contacto
    <u>{{ $document->contact }}</u>, </u> Que denuncia na qualidade de
    <u>{{ $document->complaint_quality }}</u>
</p>

<p>
    Dia <u>{{ $document->case_day }}</u>/<u>{{ $document->month }}</u>/<u>{{ $document->case_year }}</u>, Horas
    <u>{{ $document->case_time }}</u>, Lugar <u>{{ $document->case_place }}</u>, Endereço do caso:
    <u>{{ $document->case_address }}</u>
</p>
<br>


<p>
    <strong>Declara ou denuncia:</strong>
</p>
<p>
    <u>{{ $document->case_description }}</u>
</p>
