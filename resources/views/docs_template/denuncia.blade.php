<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
<HEAD>
    <META HTTP-EQUIV="CONTENT-TYPE" CONTENT="text/html; charset=utf-8">
    <TITLE></TITLE>
    <META NAME="GENERATOR" CONTENT="LibreOffice 4.1.6.2 (Linux)">
    <META NAME="AUTHOR" CONTENT="Champier Lda">
    <META NAME="CREATED" CONTENT="20201105;132300000000000">
    <META NAME="CHANGEDBY" CONTENT="Microsoft Office User">
    <META NAME="CHANGED" CONTENT="20201109;125200000000000">
    <META NAME="AppVersion" CONTENT="16.0000">
    <META NAME="DocSecurity" CONTENT="0">
    <META NAME="HyperlinksChanged" CONTENT="false">
    <META NAME="LinksUpToDate" CONTENT="false">
    <META NAME="ScaleCrop" CONTENT="false">
    <META NAME="ShareDoc" CONTENT="false">
    <STYLE TYPE="text/css">
        <!--
        @page { margin: 1in }
        P { margin-bottom: 0.08in; direction: ltr; widows: 2; orphans: 2 }
        -->
    </STYLE>
</HEAD>
<BODY LANG="pt-PT" DIR="LTR">
<P ALIGN=CENTER STYLE="margin-bottom: 0in"><IMG SRC="{{public_path('assets/images/test.png')}}" NAME="Picture 3" ALIGN=BOTTOM WIDTH=81 HEIGHT=80 BORDER=0></P>
<P ALIGN=CENTER STYLE="margin-bottom: 0in"><BR>
</P>
<P ALIGN=CENTER STYLE="margin-bottom: 0in"><FONT SIZE=4>REPÚBLICA DE
        MOÇAMBIQUE</FONT></P>
<P ALIGN=CENTER STYLE="margin-bottom: 0in; line-height: 100%"><FONT SIZE=4>MINISTÉRIO
        DO INTERIOR</FONT></P>
<P ALIGN=CENTER STYLE="margin-bottom: 0in; line-height: 100%"><FONT SIZE=4>COMANDO
        DA PRM – CIDADE DE MAPUTO</FONT></P>
<P ALIGN=CENTER STYLE="margin-bottom: 0in; line-height: 100%"><BR>
</P>
<P ALIGN=CENTER STYLE="margin-bottom: 0in; line-height: 100%"><BR>
</P>
<P ALIGN=CENTER STYLE="margin-bottom: 0in; line-height: 100%"><BR>
</P>
<P STYLE="margin-bottom: 0in; line-height: 100%"><FONT SIZE=3>Auto de
        denúncia n° <u>{{ $document->complaint_nr }}</u></FONT><FONT SIZE=3><B> PPMG ‘B”/2020
        </B></FONT><FONT SIZE=3>Unidade, 24ª Esquadra <u>{{ $document->squad_nr }}</u>, Distrito
        Municipal Kambukwana, Província <u>{{ $document->province }}</u>, Zona <u>{{ $document->zone }}</u>, Data
        <u>{{ $document->day }}</u>/<u>{{ $document->month }}</u>/<u>{{ $document->year }}</u>, Horas <u>{{ $document->time }}</u></FONT></P>
<P STYLE="margin-bottom: 0in; line-height: 100%"><BR>
</P>
<P ALIGN=CENTER STYLE="margin-bottom: 0in"><FONT SIZE=3>__________________________________________</FONT></P>
<P ALIGN=CENTER STYLE="margin-bottom: 0in; line-height: 100%"><FONT SIZE=3>(Classificação
        do Crime)</FONT></P>
<P ALIGN=CENTER STYLE="margin-bottom: 0in; line-height: 100%"><BR>
</P>
<P STYLE="margin-bottom: 0in; line-height: 115%"><FONT SIZE=3>Nesta
        subunidade apresentou-se o cidadão:
        <u>{{ $document->citizen_name }}</u> Estado Civil
        <u>{{ $document->civil_state }}</u>, Sexo <u>{{ $document->gender }}</u>, de <u>{{ $document->age }}</u> anos de idade, nascido
        (a) aos <u>{{ $document->day_birth }}</u>, do mês de <u>{{ $document->month_birth }}</u> de <u>{{ $document->year_birth }}</u>, filho
        de <u>{{ $document->name_father }} </u> e de <u>{{ $document->name_mother }}</u></FONT></P></FONT></P>
<P STYLE="margin-bottom: 0in; line-height: 115%"><FONT SIZE=3>

<P STYLE="margin-bottom: 0in; line-height: 115%"><FONT SIZE=3>Natural
        de <u>{{ $document->natural }}</u>, Distrito de <u>{{ $document->district }}</u>, Província de
        <u>{{ $document->natural_province }}</u>, Nacionalidade <u>{{ $document->nationality }}</u>, Profissão <u>{{ $document->profession }}</u></FONT>
        , Residente <u>{{ $document->residence }}</u>, Contacto <u>{{ $document->contact }}, Que
        denuncia na qualidade de
        <u>{{ $document->complaint_quality }}</u></u>
</P>

<P STYLE="margin-bottom: 0in; line-height: 115%"><BR>
</P>
<P STYLE="margin-bottom: 0in; line-height: 115%"><FONT SIZE=3>Dia
        <u>{{ $document->case_day }}</u>/<u>{{ $document->case_month }}</u>/<u>{{ $document->case_year }}</u>, Horas <u>{{ $document->case_time }}</u>, Lugar <u>{{ $document->case_place }}</u></FONT></P>
<P STYLE="margin-bottom: 0in; line-height: 115%"><A NAME="_GoBack"></A>
    <FONT SIZE=3>Endereço do caso: <u>{{ $document->case_address }}</u></FONT></P>
<P STYLE="margin-bottom: 0in; line-height: 115%"><BR>
</P>
<P STYLE="margin-bottom: 0in; line-height: 115%"><BR>
</P>
<P STYLE="margin-bottom: 0in; line-height: 115%"><BR>
</P>
<P STYLE="margin-bottom: 0in; line-height: 115%"><FONT SIZE=3><B>Declara
            ou denuncia:</B></FONT></P>
<P STYLE="margin-bottom: 0in; line-height: 115%"><FONT SIZE=3><u>{{ $document->case_description }}</u></FONT></P>
</BODY>
</HTML>
