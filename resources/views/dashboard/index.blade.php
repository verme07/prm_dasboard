@extends('layouts.master')
@section('css')
    <!-- DataTables -->
    <link href="{{ asset('assets/plugins/datatables/dataTables.bootstrap4.min.css')}}" rel="stylesheet"
          type="text/css"/>
    <link href="{{ asset('assets/plugins/datatables/buttons.bootstrap4.min.css')}}" rel="stylesheet"
          type="text/css"/>
    <!-- Responsive datatable examples -->
    <link href="{{ asset('assets/plugins/datatables/responsive.bootstrap4.min.css')}}" rel="stylesheet"
          type="text/css"/>

    <link href="{{ asset('assets/plugins/ion-rangeslider/ion.rangeSlider.skinModern.css')}}" rel="stylesheet"
          type="text/css"/>

@endsection


@section('content')
    <div class="container-fluid">

        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
                    <h4 class="page-title">Dashboard</h4>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item active">
                            Bem Vindo a PRM
                        </li>
                    </ol>
                </div>
            </div>
        </div>
        <!-- end row -->

        <div class="row">

            <div class="col-xl-4 col-md-6">
                <a href="{{ route('list-police-station') }}" class="card mini-stat bg-white">
                    <div class="card-body mini-stat-img">
                        <div class="mini-stat-icon">
                            <img src="{{ asset('assets/images/postos.png') }}" style="height: 60px;float: right;">
{{--                            <i class="fas fa-building float-right"></i>--}}
                        </div>
                        <div class="text-black">

                            <h4 class="mb-4">{{ $totals->police_stations }}</h4>
                            <h6 class="text-uppercase">Postos</h6>

                        </div>
                    </div>
                </a>
            </div>
            <div class="col-xl-4 col-md-6">
                <a href="{{ route('list-docs') }}" class="card mini-stat bg-white">
                    <div class="card-body mini-stat-img">
                        <div class="mini-stat-icon">
                            <img src="{{ asset('assets/images/docs.png') }}" style="height: 60px;float: right;">
{{--                            <i class="mdi mdi-file float-right"></i>--}}
                        </div>
                        <div class="text-black">
                            <h4 class="mb-4">{{ $totals->docs }}</h4>
                            <h6 class="text-uppercase">Documentos</h6>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-xl-4 col-md-6">
                <a href="{{ route('map-vehicle') }}" class="card mini-stat bg-white">
                    <div class="card-body mini-stat-img">
                        <div class="mini-stat-icon">
                            <img src="{{ asset('assets/images/vehicles.png') }}" style="height: 60px;float: right;">
                            {{--                            <i class="mdi mdi-car float-right"></i>--}}
                        </div>
                        <div class="text-black">
                            <h4 class="mb-4">{{ $totals->vehicles }}</h4>
                            <h6 class="text-uppercase">Veiculos</h6>
                        </div>
                    </div>
                </a>
            </div>

        </div>

        <div class="row">
            <div class="col-12">
                <div class="card m-b-20">
                    <div class="card-body">

                        <h4 class="mt-0 header-title">Listar Postos Policias</h4>

                        <table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap"
                               style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                            <thead>
                            <tr>
                                <th>#ID</th>
                                <th>Nome</th>
                                <th>Provincia</th>
                                <th>Endereço</th>
                                <th>Contacto</th>
                                <th>Telefone</th>
                                <th>Acção</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($police_stations as $police_station)
                                <tr>
                                    <td>{{ $police_station->id }}</td>
                                    <td>{{ $police_station->name }}</td>
                                    <td>{{ $police_station->province }}</td>
                                    <td>{{ $police_station->address }}</td>
                                    <td>{{ $police_station->phone1 }}</td>
                                    <td>{{ $police_station->telephone }}</td>
                                    <td>
                                        <a class="btn btn-primary"
                                           href="{{ route('edit-police-station', ['police_station_id'=>$police_station->id]) }}" >
                                            Editar
                                        </a>
                                    </td>

                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div> <!-- end col -->
        </div>


    </div> <!-- container-fluid -->
@endsection

@section('script')
    <script>
        function removerUser(user_id) {

            $('#removerModal').modal({backdrop: 'static', keyboard: false})
                .on('click', '#delete-btn', function () {
                    let user = {
                        _token: "{{ csrf_token() }}",
                        product_id: user_id,
                    }
                    $.ajax({
                        method: 'POST',
                        url: "#",
                        dataType: 'text',
                        contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
                        data: user,
                        complete: function (data) {
                            console.log("success");
                        },
                        success: function (response) {

                            let resposta = JSON.parse(response)
                            console.log(resposta)
                            if (resposta.status == 1) {
                                location.reload();
                            } else {
                                console.log("Erro")
                            }
                            ;

                        }, error: function (xhr, type, exception) {
                            console.log("Error " + xhr, type, exception + "Exception ");
                            //$('#email_error').css('display', 'block')

                        }
                    });
                })
        }
    </script>
    <!-- Required datatable js -->
    <script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js')}}"></script>
    <script src="{{ asset('assets/plugins/datatables/dataTables.bootstrap4.min.js')}}"></script>
    <!-- Buttons examples -->
    <script src="{{ asset('assets/plugins/datatables/dataTables.buttons.min.js')}}"></script>
    <script src="{{ asset('assets/plugins/datatables/buttons.bootstrap4.min.js')}}"></script>
    <script src="{{ asset('assets/plugins/datatables/jszip.min.js')}}"></script>
    <script src="{{ asset('assets/plugins/datatables/pdfmake.min.js')}}"></script>
    <script src="{{ asset('assets/plugins/datatables/vfs_fonts.js')}}"></script>
    <script src="{{ asset('assets/plugins/datatables/buttons.html5.min.js')}}"></script>
    <script src="{{ asset('assets/plugins/datatables/buttons.print.min.js')}}"></script>
    <script src="{{ asset('assets/plugins/datatables/buttons.colVis.min.js')}}"></script>
    <!-- Responsive examples -->
    <script src="{{ asset('assets/plugins/datatables/dataTables.responsive.min.js')}}"></script>
    <script src="{{ asset('assets/plugins/datatables/responsive.bootstrap4.min.js')}}"></script>

    <!-- Datatable init js -->
    <script src="{{ asset('assets/pages/datatables.init.js')}}"></script>
@endsection
