@extends('layouts.master')

@section('css')
    <link href="{{ asset('assets/plugins/dropzone/dist/dropzone.css')}}" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="{{ asset('assets/plugins/dropify/dist/css/dropify.min.css') }}">
@endsection

@section('content')
    <div class="container-fluid">

        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
                    <h4 class="page-title">Editar Posto Policial</h4>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0);">PRM</a></li>
                        <li class="breadcrumb-item"><a href="javascript:void(0);">Posto Policiais</a></li>
                        <li class="breadcrumb-item active">Editar Posto Policial</li>
                    </ol>
                </div>
            </div>
        </div>
        <!-- end row -->

        <div class="row">
            <div class="col-lg-12">
                <div class="card m-b-20">
                    <div class="card-body">

                        <h4 class="mt-0 header-title">Adicionar Activação</h4>

                        <form class="" method="POST" enctype="multipart/form-data" action="{{ route('save-edit-police-station') }}">
                            @csrf
                            <div class="row">
                                <div class="col-6">
                                    <div class="form-group">
                                        <label>Nome <span>*</span></label>
                                        <input name="police_station_id" id="police_station_id" value="{{ $police_station->id }}" type="hidden" class="form-control" required/>
                                        <input name="name" id="name" value="{{ $police_station->name }}" type="text" class="form-control" required/>
                                    </div>
                                    <div class="form-group">
                                        <label>Provincia </label>
                                        <select class="form-control" name="province" id="province" required>
                                            <option value="">Selecione a provincia</option>
                                            <option value="Maputo">Maputo</option>
                                            <option value="Gaza">Gaza</option>
                                            <option value="Inhambane">Inhambane</option>

                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Cidade <span>*</span></label>
                                        <input name="city" id="city" value="{{ $police_station->city }}"  type="text" class="form-control" required/>
                                    </div>
                                    <div class="form-group">
                                        <label>Email <span>*</span></label>
                                        <input name="email" id="email" value="{{ $police_station->email }}" type="text" class="form-control" required/>
                                    </div>

                                    <div class="form-group">
                                        <label>Endereço <span>*</span></label>
                                        <input name="address" id="address" value="{{ $police_station->address }}" type="text" class="form-control" required/>
                                    </div>
                                    <div class="form-group">
                                        <label>Descrição <span class="text-danger">*</span></label>
                                        <div class="controls">

                                            <textarea required="" name="description" class="form-control" rows="5">{{ $police_station->description }}</textarea>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-6">
                                    <div class="form-group">
                                        <label>Contacto principal<span>*</span></label>
                                        <input name="phone1" id="phone1" value="{{ $police_station->phone1 }}" type="text" class="form-control" required/>
                                    </div>
                                    <div class="form-group">
                                        <label>Contacto secundario</label>
                                        <input name="phone2" id="phone2" value="{{ $police_station->phone2 }}"  type="text" class="form-control" required/>
                                    </div>

                                    <div class="form-group">
                                        <label>Telefone <span>*</span></label>
                                        <input name="telephone" id="telephone" value="{{ $police_station->telephone }}" type="text" class="form-control" required/>
                                    </div>
                                    <div class="form-group">
                                        <label>Latitude <span>*</span></label>
                                        <input name="latitude" id="latitude" value="{{ $police_station->latitude }}" type="text" class="form-control" required/>
                                    </div>
                                    <div class="form-group">
                                        <label>Longitude <span>*</span></label>
                                        <input name="longitude" id="longitude" value="{{ $police_station->longitude }}" type="text" class="form-control" required/>
                                    </div>

                                </div>
                                <div class="col-12">
                                    <hr>
                                    <h4 class="mt-0 header-title">Imagens do Posto Policial</h4>
                                    <hr>
                                </div>
                                <div class="col-4">
                                    <div id="foto_capa1" class="card m-b-30">
                                        <img class="card-img-top img-fluid" src="{{ route('main') }}/storage/{{ $police_station->photo1 }}" alt="Card image cap">
                                        <div class="card-body">
                                            <hr>
                                            <button type="button" class="btn btn-danger waves-effect waves-light" onclick="removerFoto1()">Remover</button>
                                        </div>
                                    </div>

                                    <div class="fallback" id="input_foto1" style="display: none">
                                        <input name="photo1" id="foto1" value="0" type="file" class="dropify">
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div id="foto_capa2" class="card m-b-30">
                                        <img class="card-img-top img-fluid" src="{{ route('main') }}/storage/{{ $police_station->photo2 }}" alt="Card image cap">
                                        <div class="card-body">
                                            <hr>
                                            <button type="button" class="btn btn-danger waves-effect waves-light" onclick="removerFoto2()">Remover</button>
                                        </div>
                                    </div>

                                    <div class="fallback" id="input_foto2" style="display: none">
                                        <input name="photo2" id="foto2" value="0" type="file" class="dropify">
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div id="foto_capa3" class="card m-b-30">
                                        <img class="card-img-top img-fluid" src="{{ route('main') }}/storage/{{ $police_station->photo3 }}" alt="Card image cap">
                                        <div class="card-body">
                                            <hr>
                                            <button type="button" class="btn btn-danger waves-effect waves-light" onclick="removerFoto3()">Remover</button>
                                        </div>
                                    </div>

                                    <div class="fallback" id="input_foto3" style="display: none">
                                        <input name="photo3" id="foto3" value="0" type="file" class="dropify">
                                    </div>
                                </div>
                                <br>
                                <br>
                            </div>
                            <div class="form-group">
                                <div>
                                    <button type="submit" class="btn btn-primary waves-effect waves-light">
                                        Adicionar
                                    </button>

                                </div>
                            </div>
                        </form>

                    </div>
                </div>
            </div> <!-- end col -->

        </div> <!-- end row -->

    </div> <!-- container-fluid -->
@endsection

@section('script')

    <script>
        function removerFoto1() {
            $('#foto_capa1').css('display', 'none');
            $('#input_foto1').css('display', 'block');
        }
        function removerFoto2() {
            $('#foto_capa2').css('display', 'none');
            $('#input_foto2').css('display', 'block');
        }
        function removerFoto3() {
            $('#foto_capa3').css('display', 'none');
            $('#input_foto3').css('display', 'block');
        }
    </script>
    <!-- Parsley js -->
    <script src="{{ URL::asset('assets/plugins/parsleyjs/parsley.min.js')}}"></script>
@endsection

@section('script-bottom')
    <script>
        $(document).ready(function() {
            $('form').parsley();
        });
    </script>
    <script src="{{ asset('assets/plugins/dropify/dist/js/dropify.min.js') }}"></script>
    <script>
        $(document).ready(function () {
            // Basic
            $('.dropify').dropify();

            // Translated
            $('.dropify-fr').dropify({
                messages: {
                    default: 'Glissez-déposez un fichier ici ou cliquez',
                    replace: 'Glissez-déposez un fichier ou cliquez pour remplacer',
                    remove: 'Supprimer',
                    error: 'Désolé, le fichier trop volumineux'
                }
            });

            // Used events
            var drEvent = $('#input-file-events').dropify();

            drEvent.on('dropify.beforeClear', function (event, element) {
                return confirm("Do you really want to delete \"" + element.file.name + "\" ?");
            });

            drEvent.on('dropify.afterClear', function (event, element) {
                alert('File deleted');
            });

            drEvent.on('dropify.errors', function (event, element) {
                console.log('Has Errors');
            });

            var drDestroy = $('#input-file-to-destroy').dropify();
            drDestroy = drDestroy.data('dropify')
            $('#toggleDropify').on('click', function (e) {
                e.preventDefault();
                if (drDestroy.isDropified()) {
                    drDestroy.destroy();
                } else {
                    drDestroy.init();
                }
            })
        });
    </script>
@endsection
