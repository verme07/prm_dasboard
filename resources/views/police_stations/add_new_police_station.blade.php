@extends('layouts.master')

@section('css')
    <link href="{{ asset('assets/plugins/dropzone/dist/dropzone.css')}}" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="{{ asset('assets/plugins/dropify/dist/css/dropify.min.css') }}">
@endsection

@section('content')
    <div class="container-fluid">

        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
                    <h4 class="page-title">Adicionar Posto Policial</h4>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0);">PRM</a></li>
                        <li class="breadcrumb-item"><a href="javascript:void(0);">Posto Policiais</a></li>
                        <li class="breadcrumb-item active">Adicionar Posto Policial</li>
                    </ol>
                </div>
            </div>
        </div>
        <!-- end row -->

        <div class="row">
            <div class="col-lg-12">
                <div class="card m-b-20">
                    <div class="card-body">

                        <h4 class="mt-0 header-title">Adicionar Posto Policial</h4>

                        <form class="" method="POST" enctype="multipart/form-data" action="{{ route('save-police-station') }}">
                            @csrf
                            <div class="row">
                                <div class="col-6">
                                    <div class="form-group">
                                        <label>Nome <span>*</span></label>
                                             <input name="name" id="name " type="text" class="form-control" required/>
                                    </div>
                                    <div class="form-group">
                                        <label>Provincia </label>
                                        <select class="form-control" name="province" id="province" required>
                                            <option value="">Selecione a provincia</option>
                                            <option value="Maputo">Maputo</option>
                                            <option value="Gaza">Gaza</option>
                                            <option value="Inhambane">Inhambane</option>

                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Cidade <span>*</span></label>
                                        <input name="city" id="city " type="text" class="form-control" required/>
                                    </div>
                                    <div class="form-group">
                                        <label>Email <span>*</span></label>
                                        <input name="email" id="email " type="text" class="form-control" required/>
                                    </div>
                                    <div class="form-group">
                                        <label>Endereço <span>*</span></label>
                                        <input name="address" id="address " type="text" class="form-control" required/>
                                    </div>

                                    <div class="form-group">
                                        <label>Descrição <span class="text-danger">*</span></label>
                                        <div class="controls">

                                            <textarea required="" name="description" class="form-control" rows="5"></textarea>
                                        </div>
                                    </div>

                                </div>

                                <div class="col-6">
                                    <div class="form-group">
                                        <label>Contacto principal<span>*</span></label>
                                        <input name="phone1" id="phone1 " type="text" class="form-control" required/>
                                    </div>
                                    <div class="form-group">
                                        <label>Contacto secundario</label>
                                        <input name="phone2" id="phone2 " type="text" class="form-control" required/>
                                    </div>

                                    <div class="form-group">
                                        <label>Telefone <span>*</span></label>
                                        <input name="telephone" id="telephone " type="text" class="form-control" required/>
                                    </div>
                                    <div class="form-group">
                                        <label>Latitude <span>*</span></label>
                                        <input name="latitude" id="latitude " type="text" class="form-control" required/>
                                    </div>
                                    <div class="form-group">
                                        <label>Longitude <span>*</span></label>
                                        <input name="longitude" id="longitude " type="text" class="form-control" required/>
                                    </div>

                                </div>

                                    <div class="col-12">
                                        <hr>
                                        <h4 class="mt-0 header-title">Imagens do Posto Policial</h4>
                                        <hr>
                                    </div>


                                    <div class="col-4">
                                        <div class="form-group">
                                            <div class="fallback">
                                                <label>Imagem 1 </label>
                                                <input name="photo1" id="photo1" type="file" class="dropify">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <div class="form-group">
                                            <div class="fallback">
                                                <label>Imagem 2 </label>
                                                <input name="photo2" id="photo2" type="file" class="dropify">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <div class="form-group">
                                            <div class="fallback">
                                                <label>Imagem 3 </label>
                                                <input name="photo3" id="photo3" type="file" class="dropify">
                                            </div>
                                        </div>
                                    </div>

                                <br>
                            </div>
                            <div class="form-group">
                                <div>
                                    <button type="submit" class="btn btn-primary waves-effect waves-light">
                                        Adicionar
                                    </button>
                                    <button type="reset" class="btn btn-secondary waves-effect m-l-5">
                                        Cancelar
                                    </button>
                                </div>
                            </div>
                        </form>

                    </div>
                </div>
            </div> <!-- end col -->

        </div> <!-- end row -->

    </div> <!-- container-fluid -->
@endsection

@section('script')
    <!-- Parsley js -->
    <script src="{{ URL::asset('assets/plugins/parsleyjs/parsley.min.js')}}"></script>
@endsection

@section('script-bottom')
    <script>
        $(document).ready(function() {
            $('form').parsley();
        });
    </script>
    <script src="{{ asset('assets/plugins/dropify/dist/js/dropify.min.js') }}"></script>
    <script>
        $(document).ready(function () {
            // Basic
            $('.dropify').dropify();

            // Translated
            $('.dropify-fr').dropify({
                messages: {
                    default: 'Glissez-déposez un fichier ici ou cliquez',
                    replace: 'Glissez-déposez un fichier ou cliquez pour remplacer',
                    remove: 'Supprimer',
                    error: 'Désolé, le fichier trop volumineux'
                }
            });

            // Used events
            var drEvent = $('#input-file-events').dropify();

            drEvent.on('dropify.beforeClear', function (event, element) {
                return confirm("Do you really want to delete \"" + element.file.name + "\" ?");
            });

            drEvent.on('dropify.afterClear', function (event, element) {
                alert('File deleted');
            });

            drEvent.on('dropify.errors', function (event, element) {
                console.log('Has Errors');
            });

            var drDestroy = $('#input-file-to-destroy').dropify();
            drDestroy = drDestroy.data('dropify')
            $('#toggleDropify').on('click', function (e) {
                e.preventDefault();
                if (drDestroy.isDropified()) {
                    drDestroy.destroy();
                } else {
                    drDestroy.init();
                }
            })
        });
    </script>
@endsection
