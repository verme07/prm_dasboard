@extends('layouts.master-without-nav')

@section('content')

    <div class="wrapper-page">

        <div class="card" style="background-color: #fff0; padding-top: 25%;     box-shadow: 0 -3px 31px 0 rgb(0 0 0 / 0%), 0 6px 20px 0 rgb(0 0 0 / 0%);">
            <div class="card-body" style="padding-top: 25%">

                <div class="p-3">
{{--                    <h4 class="font-18 m-b-5 text-center text-white">Bem Vindo!</h4>--}}
{{--                    <p class="text-center text-white">Login para continuar.</p>--}}

                    <form class="form-horizontal m-t-30" method="POST" action="{{ route('login') }}">
                        @csrf
                        <div class="form-group">
{{--                            <label class="text-white" for="username">NUI</label>--}}
                            <input type="text"  class="form-control @error('username') is-invalid @enderror" name="username" value="{{ old('username') }}" required id="username" placeholder="NIP">
                            @error('username')
                            <span class="text-white invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>

                        <div class="form-group">
{{--                            <label class="text-white" for="password">Password</label>--}}
                            <input type="password" class="form-control @error('password') is-invalid @enderror" id="password" name="password" required autocomplete="current-password" placeholder="Password" >
                            @error('password')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>

                        <div class="form-group row m-t-20">
                            <div class="col-12 text-center">
                                <button class="btn btn-primary w-md waves-effect waves-light" type="submit">Entrar</button>
                            </div>
                        </div>

{{--                        <div class="form-group m-t-10 mb-0 row">--}}
{{--                            <div class="col-12 m-t-20">--}}
{{--                                <a href="#" class="text-white"><i class="mdi mdi-lock"></i> Esqueceu a password?</a>--}}
{{--                            </div>--}}
{{--                        </div>--}}
                    </form>
                </div>

            </div>
        </div>

        <div class="m-t-40 text-center text-white">
            <p>© {{date('Y')}} PRM.</p>
        </div>

    </div>

@endsection
