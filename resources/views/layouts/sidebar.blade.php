            <!-- ========== Left Sidebar Start ========== -->
            <div class="left side-menu">
                <div class="slimscroll-menu" id="remove-scroll">

                    <!--- Sidemenu -->
                    <div id="sidebar-menu">
{{--                        <h6 style="text-align: center"> 1o Posto Policial da PRM</h6>--}}
                        <!-- Left Menu Start -->
                        <ul class="metismenu" id="side-menu">

{{--                            <hr class="menu-title">Main</hr>--}}
                            <hr>
                            <li>
                                <a href="{{ route('main') }}" class="waves-effect">
                                    <i class="mdi mdi-view-dashboard"></i> <span> Dashboard </span>
                                </a>
                            </li>


                            <li>
                                <a href="javascript:void(0);" class="waves-effect"><i class="mdi mdi mdi-car"></i><span> Viaturas <span class="float-right menu-arrow"><i class="mdi mdi-chevron-right"></i></span> </span></a>
                                <ul class="submenu">
                                    <li><a href="{{ route('add-vehicle') }}">Adicionar</a></li>
                                    <li><a href="{{ route('list-vehicle') }}">Gerir</a></li>
                                    <li><a href="{{ route('map-vehicle') }}">Mapa</a></li>
                                    <li><a href="{{ route('generate-report') }}">Historico</a></li>
                                </ul>
                            </li>

                            <li>
                                <a href="javascript:void(0);" class="waves-effect"><i class="fas fa-building"></i><span> Postos Policiais e Esquadras <span class="float-right menu-arrow"><i class="mdi mdi-chevron-right"></i></span> </span></a>
                                <ul class="submenu">
                                    <li><a href="{{ route('add-police-station') }}">Adicionar</a></li>
                                    <li><a href="{{ route('list-police-station') }}">Gerir</a></li>
                                    <li><a href="{{ route('reviews') }}">Classificações</a></li>
                                </ul>
                            </li>

                            <li>
                                <a href="javascript:void(0);" class="waves-effect"><i class="fas fa-envelope"></i><span> Mensagens <span class="float-right menu-arrow"><i class="mdi mdi-chevron-right"></i></span> </span></a>
                                <ul class="submenu">
                                    <li><a href="{{ route('chat', ['chat_id'=>1]) }}">Mensagens</a></li>
                                </ul>
                            </li>

                            <li>
                                <a href="javascript:void(0);" class="waves-effect"><i class="fas fa-file-word"></i><span> Documentos <span class="float-right menu-arrow"><i class="mdi mdi-chevron-right"></i></span> </span></a>
                                <ul class="submenu">
                                    <li><a href="{{ route('add-doc') }}">Gerar Documento</a></li>
                                    <li><a href="{{ route('list-docs') }}">Lista de Documentos</a></li>
                                </ul>
                            </li>

                            <li>
                                <a href="javascript:void(0);" class="waves-effect"><i class="fas fa-users"></i><span> Utilizadores <span class="float-right menu-arrow"><i class="mdi mdi-chevron-right"></i></span> </span></a>
                                <ul class="submenu">
                                    <li><a href="{{  route('add-user') }}">Adicionar</a></li>
                                    <li><a href="{{ route('list-user') }}">Gerir</a></li>
                                    <li><a href="{{ route('list-app-user') }}">Utizadores da App</a></li>
                                </ul>
                            </li>



                        </ul>

                    </div>
                    <!-- Sidebar -->
                    <div class="clearfix"></div>

                </div>
                <!-- Sidebar -left -->

            </div>
            <!-- Left Sidebar End -->
