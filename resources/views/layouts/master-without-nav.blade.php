<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
        <title>PRM</title>
        @include('layouts.head')
        <style>
            #color-overlay {
                position: absolute;
                top: 0;
                left: 0;
                width: 100%;
                height: 100%;
                background-color: #ffffff;
                opacity: 0.6;
            }
        </style>
  </head>
<body>
    <!-- Begin page -->
        <div id="wrapper" style="background-image: url('http://3.134.70.3/prm_dashboard/public//assets/images/logo.png');background-size: contain;background-repeat: no-repeat;background-position: center;">
            <div id="color-overlay"></div>
                @yield('content')


        </div>
    @include('layouts.footer-script')    
    </body>
</html>
