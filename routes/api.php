<?php

use App\Http\Controllers\LocationsController;
use App\Http\Controllers\MessageController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/register', [App\Http\Controllers\UserController::class, 'storeApi']);
Route::post('/login', [App\Http\Controllers\UserController::class, 'loginApi']);
Route::post('/edit-user', [App\Http\Controllers\UserController::class, 'editApi']);
Route::post('/edit-user-ios', [App\Http\Controllers\UserController::class, 'editApiIOS']);
Route::post('/update-password', [App\Http\Controllers\UserController::class, 'updatePassApi']);

Route::post('/add-review', [App\Http\Controllers\PoliceStationReviewController::class, 'store']);
Route::get('/get-reviews/{police_station_id}', [App\Http\Controllers\PoliceStationReviewController::class, 'getReviews']);

Route::get('/all-police-stations', [App\Http\Controllers\PoliceStationController::class, 'retrivePoliceStations'])->name('all-police-stations');
Route::get('/get-chats', [App\Http\Controllers\ConversationController::class, 'getConversations'])->name('get-chats');
Route::get('/get-conversation/{chat_id}', [App\Http\Controllers\ConversationController::class, 'getMessageChat'])->name('get-chats');

Route::get('/get-sent-messages/{user_id}', [App\Http\Controllers\ConversationController::class, 'getSendMessages'])->name('get-sent-messages');
Route::get('/get-received-messages/{user_id}', [App\Http\Controllers\ConversationController::class, 'getReceivedMessages'])->name('get-received-messages');

Route::post('/sent-message', [App\Http\Controllers\MessageController::class, 'store']);
Route::post('/get-car-report', [App\Http\Controllers\LocationsController::class, 'locationHistory']);
