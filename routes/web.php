<?php

use App\Http\Controllers\ConversationController;
use App\Http\Controllers\DocumentController;
use App\Http\Controllers\LocationsController;
use App\Http\Controllers\MessageController;
use App\Http\Controllers\PoliceStationController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\VehicleController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('dashboard.index');
//});
//
//
//Route::get('/dashboard', function () {
//    return view('dashboard.index');
//});

Auth::routes();



Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('main');
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::get('/add-police-station', [App\Http\Controllers\PoliceStationController::class, 'addStation'])->name('add-police-station');
Route::get('/list-police-station', [App\Http\Controllers\PoliceStationController::class, 'list'])->name('list-police-station');
Route::post('/save-police-station', [App\Http\Controllers\PoliceStationController::class, 'store'])->name('save-police-station');
Route::post('/save-edit-police-station', [App\Http\Controllers\PoliceStationController::class, 'edit'])->name('save-edit-police-station');
Route::get('/edit-police-station/{police_station_id}', [App\Http\Controllers\PoliceStationController::class, 'policeStation'])->name('edit-police-station');
Route::post('/remove-police-station', [App\Http\Controllers\PoliceStationController::class, 'remove'])->name('remove-police-station');

Route::get('/add-vehicle', [App\Http\Controllers\VehicleController::class, 'addVehicle'])->name('add-vehicle');
Route::get('/list-vehicle', [App\Http\Controllers\VehicleController::class, 'list'])->name('list-vehicle');
Route::post('/save-vehicle', [App\Http\Controllers\VehicleController::class, 'store'])->name('save-vehicle');
Route::get('/edit-vehicle/{vehicle_id}', [App\Http\Controllers\VehicleController::class, 'viewCarEdit'])->name('edit-vehicle');
Route::post('/save-edit-vehicle', [App\Http\Controllers\VehicleController::class, 'edit'])->name('save-edit-vehicle');
Route::post('/remove-vehicle', [App\Http\Controllers\VehicleController::class, 'remove'])->name('remove-vehicle');

Route::get('/map-vehicle', [App\Http\Controllers\VehicleController::class, 'map'])->name('map-vehicle');

Route::get('/add-user', [App\Http\Controllers\UserController::class, 'addUser'])->name('add-user');
Route::post('/save-user', [App\Http\Controllers\UserController::class, 'store'])->name('save-user');
Route::post('/remove-user', [App\Http\Controllers\UserController::class, 'remove'])->name('remove-user');

Route::get('/list-user', [App\Http\Controllers\UserController::class, 'listUser'])->name('list-user');
Route::get('/list-app-user', [App\Http\Controllers\UserController::class, 'listAppUsers'])->name('list-app-user');

Route::get('/edit-user/{user_id}', [App\Http\Controllers\UserController::class, 'editUser'])->name('edit-user');
Route::post('/save-user-edit', [App\Http\Controllers\UserController::class, 'edit'])->name('save-user-edit');

Route::get('/add-doc', [App\Http\Controllers\DocumentController::class, 'index'])->name('add-doc');
Route::get('/list-docs', [App\Http\Controllers\DocumentController::class, 'listAutoDenuncia'])->name('list-docs');
Route::get('/generate-a-denuncia-pdf/{doc_id}', [App\Http\Controllers\DocumentController::class, 'generatePDF'])->name('generate-a-denuncia-pdf');
Route::get('/add-doc-auto-denuncia', [App\Http\Controllers\DocumentController::class, 'addAutoDenuncia'])->name('add-doc-auto-denuncia');
Route::post('/save-auto-denuncia', [App\Http\Controllers\DocumentController::class, 'addAutoDeDenuncia'])->name('save-auto-denuncia');
Route::post('/remove-auto-denuncia', [App\Http\Controllers\DocumentController::class, 'remove'])->name('remove-auto-denuncia');

Route::get('/get-document/{doc_id}', [App\Http\Controllers\DocumentController::class, 'getDocument'])->name('get-document');
Route::get('/duplicate-document/{doc_id}', [App\Http\Controllers\DocumentController::class, 'duplicateEdit'])->name('duplicate-document');
Route::post('/edit-auto-denuncia', [App\Http\Controllers\DocumentController::class, 'edit'])->name('edit-auto-denuncia');


Route::get('/chat/{chat_id}', [App\Http\Controllers\ConversationController::class, 'index'])->name('chat');
Route::post('/answer-message', [App\Http\Controllers\MessageController::class, 'answer'])->name('answer-message');

Route::get('/generate-report', [App\Http\Controllers\LocationsController::class, 'generateHistory'])->name('generate-report');

Route::get('/reviews', [App\Http\Controllers\PoliceStationReviewController::class, 'index'])->name('reviews');
