<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVehiclesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vehicles', function (Blueprint $table) {
            $table->id();
            $table->string('imei');
            $table->string('engine_nr')->nullable();
            $table->string('brand');
            $table->string('reference');
            $table->string('number_plate');
            $table->integer('space');
            $table->string('gas_type');
            $table->integer('status')->default(0);
            $table->string('latitude')->nullable();
            $table->string('longitude')->nullable();
            $table->double('direction')->nullable();
            $table->double('speed')->nullable();
            $table->dateTime('gprs_time')->nullable();

            $table->bigInteger('police_station_id')->unsigned();
            $table->foreign('police_station_id')->references('id')->on('police_stations')->onUpdate('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vehicles');
    }
}
