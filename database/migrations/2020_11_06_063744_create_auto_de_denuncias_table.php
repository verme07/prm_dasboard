<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAutoDeDenunciasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('auto_de_denuncias', function (Blueprint $table) {
            $table->id();
            $table->string('complaint_nr')->nullable();
            $table->string('squad_nr')->nullable();
            $table->string('province')->nullable();
            $table->string('zone')->nullable();
            $table->integer('day')->nullable();
            $table->integer('month')->nullable();
            $table->integer('year')->nullable();
            $table->string('time')->nullable();
            $table->string('name')->nullable();
            $table->string('civil_state')->nullable();
            $table->string('gender')->nullable();
            $table->integer('age')->nullable();
            $table->integer('day_birth')->nullable();
            $table->string('month_birth')->nullable();
            $table->integer('year_birth')->nullable();
            $table->string('name_father')->nullable();
            $table->string('name_mother')->nullable();
            $table->string('natural')->nullable();
            $table->string('district')->nullable();
            $table->string('natural_province')->nullable();
            $table->string('nationality')->nullable();
            $table->string('profession')->nullable();
            $table->string('residence')->nullable();
            $table->string('complaint_quality')->nullable();

            $table->integer('case_day')->nullable();
            $table->integer('case_month')->nullable();
            $table->integer('case_year')->nullable();
            $table->string('case_time')->nullable();
            $table->string('case_place')->nullable();
            $table->string('case_address')->nullable();
            $table->text('case_description')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('auto_de_denuncias');
    }
}
